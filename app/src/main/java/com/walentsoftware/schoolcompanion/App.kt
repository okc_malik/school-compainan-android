package com.walentsoftware.schoolcompanion

import android.app.Application
import android.content.Context
import com.walentsoftware.schoolcompanion.data.theme.PreferenceRepository

class App : Application() {

    lateinit var preferenceRepository: PreferenceRepository

    override fun onCreate() {
        super.onCreate()
        preferenceRepository =
            PreferenceRepository(
                getSharedPreferences(DEFAULT_PREFERENCES, Context.MODE_PRIVATE)
            )
    }

    companion object {
        const val DEFAULT_PREFERENCES = "default_preferences"
    }
}
