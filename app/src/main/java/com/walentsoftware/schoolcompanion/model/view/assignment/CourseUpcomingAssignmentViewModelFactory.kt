package com.walentsoftware.schoolcompanion.model.view.assignment

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider

class CourseUpcomingAssignmentViewModelFactory : ViewModelProvider.Factory {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(CourseUpcomingAssignmentViewModel::class.java)) {
            return CourseUpcomingAssignmentViewModel() as T
        }
        throw IllegalArgumentException("Unknown ViewModel class")
    }
}