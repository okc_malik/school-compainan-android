package com.walentsoftware.schoolcompanion.model.view.agenda

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider

class ScoreViewModelFactory : ViewModelProvider.Factory {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(AgendaViewModel::class.java)) {
            return AgendaViewModel() as T
        }
        throw IllegalArgumentException("Unknown ViewModel class")
    }
}