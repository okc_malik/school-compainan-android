package com.walentsoftware.schoolcompanion.model.data

data class CourseUpcomingAssignment(
    val id: String,
    val name: String,
    val priority: Int = 1,
    val desc: String?,
    val timestamp: Long,
    val isDark: Boolean = false,
    val dueDate: Long,
    val color: Int,
    val strokeColor: Int = color
)