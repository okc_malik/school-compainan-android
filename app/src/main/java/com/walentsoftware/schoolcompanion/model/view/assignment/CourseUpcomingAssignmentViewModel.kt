package com.walentsoftware.schoolcompanion.model.view.assignment

import androidx.lifecycle.ViewModel
import com.walentsoftware.schoolcompanion.activities.home.ui.agenda.DefaultAgendaRepository
import com.walentsoftware.schoolcompanion.model.data.CourseUpcomingAssignment

class CourseUpcomingAssignmentViewModel : ViewModel() {

    val loadUpcomingAssignments: List<CourseUpcomingAssignment>


    init {


        // Load agenda blocks.

        val observableAgenda = DefaultAgendaRepository()
            .generateAssignments()

        loadUpcomingAssignments = observableAgenda

        loadUpcomingAssignments.toString()
    }
}