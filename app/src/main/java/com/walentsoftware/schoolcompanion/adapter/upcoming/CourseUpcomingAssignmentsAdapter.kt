package com.walentsoftware.schoolcompanion.adapter.upcoming

import android.util.SparseBooleanArray
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Filter
import android.widget.Filterable
import androidx.core.util.isEmpty
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.recyclerview.widget.RecyclerView
import com.walentsoftware.schoolcompanion.R
import com.walentsoftware.schoolcompanion.interfaces.CourseUpcomingAssignmentListener
import com.walentsoftware.schoolcompanion.model.data.CourseUpcomingAssignment


class CourseUpcomingAssignmentsAdapter(
    private val listener: CourseUpcomingAssignmentListener,
    private val list: List<CourseUpcomingAssignment>
) : RecyclerView.Adapter<CourseUpcomingAssignmentsViewHolder>(), Filterable {

    private val selectedItems: SparseBooleanArray = SparseBooleanArray()
    private var currentSelectedIndex = -1
    private var upFilteredList: List<CourseUpcomingAssignment> = list


    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): CourseUpcomingAssignmentsViewHolder {

        return CourseUpcomingAssignmentsViewHolder(
            DataBindingUtil.inflate(LayoutInflater.from(parent.context), viewType, parent, false)

        )
    }

    override fun onBindViewHolder(holder: CourseUpcomingAssignmentsViewHolder, position: Int) {
        holder.bind(upFilteredList[position])
        val icon = holder.itemView.findViewById<View>(R.id.selectedIcon)
        if (selectedItems.isEmpty()) {
            icon.visibility = View.INVISIBLE
        } else {
            if (selectedItems[position, false]) {
                //animationItemsIndex.delete(pos)
                //icon.imageView.setImageResource(0)
                icon.visibility = View.VISIBLE
            } else {
                icon.visibility = View.INVISIBLE

            }
        }

        applyClickEvents(holder, position)
    }

    override fun getItemViewType(position: Int): Int {
        return if (upFilteredList[position].isDark) {
            R.layout.item_course_upcoming_assignment_dark
        } else {
            R.layout.item_course_upcoming_assignment_light
        }
    }

    private fun applyClickEvents(holder: CourseUpcomingAssignmentsViewHolder, position: Int) {

        holder.itemView.findViewById<View>(R.id.backgroundView).setOnClickListener {


            listener.onRowClicked(
                position
            )
        }

        holder.itemView.findViewById<View>(R.id.icon).setOnClickListener {

            listener.onIconClicked(
                position
            )
        }

        holder.itemView.findViewById<View>(R.id.backgroundView).setOnLongClickListener {
            listener.onLongRowClicked(
                position
            )
            return@setOnLongClickListener true
        }
    }

    override fun getItemCount(): Int {
        return upFilteredList.size
    }

    fun toggleSelection(pos: Int) {
        currentSelectedIndex = pos
        if (selectedItems[pos, false]) {
            selectedItems.delete(pos)
            //animationItemsIndex.delete(pos)

        } else {
            selectedItems.put(pos, true)
            //animationItemsIndex.put(pos, true)
        }
        notifyItemChanged(pos)
    }

    fun clearSelections() {
        //reverseAllAnimations = true
        selectedItems.clear()
        notifyDataSetChanged()
    }

    fun getSelectedItemCount(): Int {
        return selectedItems.size()
    }

    fun getSelectedItems(): List<Int>? {
        val items: MutableList<Int> = ArrayList(selectedItems.size())
        for (i in 0 until selectedItems.size()) {
            items.add(selectedItems.keyAt(i))
        }
        return items
    }

    override fun getFilter(): Filter? {
        return object : Filter() {
            override fun performFiltering(charSequence: CharSequence): FilterResults {
                val charString = charSequence.toString()
                if (charString.isEmpty()) {
                    upFilteredList = list
                } else {
                    val filteredList: MutableList<CourseUpcomingAssignment> = ArrayList()
                    for (row in list) {

                        // name match condition. this might differ depending on your requirement
                        // here we are looking for name or phone number match
                        if (row.name.toLowerCase()
                                .contains(charString.toLowerCase())
                        ) {
                            filteredList.add(row)
                        }
                    }
                    upFilteredList = filteredList
                }
                val filterResults = FilterResults()
                filterResults.values = upFilteredList
                return filterResults
            }

            override fun publishResults(
                charSequence: CharSequence,
                filterResults: FilterResults
            ) {
                upFilteredList = filterResults.values as ArrayList<CourseUpcomingAssignment>
                notifyDataSetChanged()
            }
        }
    }
}

class CourseUpcomingAssignmentsViewHolder(
    private val binding: ViewDataBinding
) : RecyclerView.ViewHolder(binding.root) {
    fun bind(assignment: CourseUpcomingAssignment) {

        binding.setVariable(1, assignment)
        binding.executePendingBindings()
    }
}
