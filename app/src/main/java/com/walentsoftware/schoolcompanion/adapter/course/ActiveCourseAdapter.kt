package com.walentsoftware.schoolcompanion.adapter.course

import android.graphics.Color
import android.graphics.drawable.GradientDrawable
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.annotation.ColorInt
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.graphics.ColorUtils
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.card.MaterialCardView
import com.walentsoftware.schoolcompanion.R
import com.walentsoftware.schoolcompanion.data.course.ActiveCourse
import com.walentsoftware.schoolcompanion.extension.inflate
import com.walentsoftware.schoolcompanion.interfaces.ActiveCourseAdapterListener


class ActiveCourseAdapter(private val courseList: ArrayList<ActiveCourse>, private val listener: ActiveCourseAdapterListener): RecyclerView.Adapter<ActiveCourseAdapter.MyViewHolder>() {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val inflatedView = parent.inflate(R.layout.home_active_course_list_item, false)
        return MyViewHolder(
            inflatedView
        )

    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        val sport: ActiveCourse = courseList[position]
        val colorOne = Color.parseColor("#${sport.colorOne}")
        val colorTwo = Color.parseColor("#${sport.colorTwo}")

        holder.courseName.text = sport.courseName


        if (colorOne.isDark()) {
            holder.courseName.setTextColor(Color.parseColor("#ffffff"))
        } else {
            holder.courseName.setTextColor(Color.parseColor("#000000"))
        }


        val gd = GradientDrawable(
            GradientDrawable.Orientation.TOP_BOTTOM, intArrayOf(colorOne, colorTwo)
        )
        gd.cornerRadius = 0f

        holder.gradientBg.setBackgroundDrawable(gd)


        // apply click events
        applyClickEvents(holder, position)

    }

    private fun @receiver:ColorInt Int.isDark(): Boolean =
        ColorUtils.calculateLuminance(this) < 0.5


    override fun getItemCount(): Int {
        return courseList.size
    }

    private fun applyClickEvents(holder: MyViewHolder, position: Int) {

        holder.courseCard.setOnClickListener {
            listener.onCourseRowClicked(
                position
            )
        }
    }

    class MyViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        var courseName: TextView
        var courseCard: MaterialCardView
        var gradientBg: ConstraintLayout

        init {
            courseCard = view.findViewById(R.id.courseCard)
            courseName = view.findViewById(R.id.courseName)
            gradientBg = view.findViewById(R.id.gradientBg)
        }

    }


}