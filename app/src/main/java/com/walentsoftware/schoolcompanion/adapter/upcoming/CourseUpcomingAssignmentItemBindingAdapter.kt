package com.walentsoftware.schoolcompanion.adapter.upcoming

import android.graphics.drawable.GradientDrawable
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import androidx.appcompat.content.res.AppCompatResources
import androidx.databinding.BindingAdapter
import com.walentsoftware.schoolcompanion.R

@BindingAdapter(
    value = ["assignmentColor", "assignmentStrokeColor", "assignmentStrokeWidth"], requireAll = true
)
fun assignmentColor(view: View, fillColor: Int, strokeColor: Int, strokeWidth: Float) {
    view.background = (view.background as? GradientDrawable ?: GradientDrawable()).apply {
        setColor(fillColor)
        setStroke(strokeWidth.toInt(), strokeColor)
    }
}

@BindingAdapter("assignmentIcon")
fun assignmentIcon(imageView: ImageView, type: Int) {
    val iconId = when (type) {
        0 -> R.drawable.ic_priority_low
        1 -> R.drawable.ic_priority_med
        2 -> R.drawable.ic_priority_high
        else -> R.drawable.ic_priority_low
    }
    imageView.setImageDrawable(AppCompatResources.getDrawable(imageView.context, iconId))
}

@BindingAdapter(value = ["assignmentDesc"], requireAll = true)
fun assignmentDesc(
    textView: TextView,
    desc: String?
) {

    textView.text = desc.toString()
}
