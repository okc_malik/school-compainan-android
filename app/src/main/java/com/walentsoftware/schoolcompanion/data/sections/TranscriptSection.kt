package com.walentsoftware.schoolcompanion.data.sections

import android.graphics.Color
import android.view.View
import androidx.recyclerview.widget.RecyclerView
import com.walentsoftware.schoolcompanion.R
import com.walentsoftware.schoolcompanion.data.CourseListItem
import com.walentsoftware.schoolcompanion.data.TranscriptViewHolder
import com.walentsoftware.schoolcompanion.shared.HeaderViewHolder
import io.github.luizgrp.sectionedrecyclerviewadapter.Section
import io.github.luizgrp.sectionedrecyclerviewadapter.SectionParameters

class TranscriptSection internal constructor(
    private val title: String, position: Int, list: ArrayList<CourseListItem>,
    clickListener: ClickListener
) :
    Section(
        SectionParameters.builder()
            .itemResourceId(R.layout.transcript_list_row)
            .headerResourceId(R.layout.transcript_section_header)
            .build()
    ) {
    private val list: ArrayList<CourseListItem> = list
    private val clickListener: ClickListener = clickListener
    var isExpanded = true

    override fun getContentItemsTotal(): Int {
        return if (isExpanded) list.size else 0
    }

    override fun getItemViewHolder(view: View): RecyclerView.ViewHolder {
        return TranscriptViewHolder(view)
    }

    override fun onBindItemViewHolder(
        holder: RecyclerView.ViewHolder,
        position: Int
    ) {
        val itemHolder: TranscriptViewHolder = holder as TranscriptViewHolder
        val course: CourseListItem = list[position]
        itemHolder.courseName.text = course.name
        itemHolder.credits.text = "${course.hours} credit hours"
        //itemHolder.icon.setBackgroundColor(R.drawable.bg_circle)
        itemHolder.iconText.visibility = View.VISIBLE
        itemHolder.container.setOnClickListener {
            clickListener.onItemRootViewClicked(
                title,
                itemHolder.adapterPosition
            )
        }

        if (course.grade!! >= 3.7) {
            itemHolder.icon.setImageResource(R.drawable.bg_circle)
            itemHolder.icon.setColorFilter(Color.parseColor("#5ec639"))
            itemHolder.iconText.setTextColor(Color.parseColor("#ffffff"))
        } else if (course.grade <= 3.30 && course.grade > 2.30) {
            itemHolder.icon.setImageResource(R.drawable.bg_circle)
            itemHolder.icon.setColorFilter(Color.parseColor("#ffeb00"))
            itemHolder.iconText.setTextColor(Color.parseColor("#000000"))
        } else {
            itemHolder.icon.setImageResource(R.drawable.bg_circle)
            itemHolder.icon.setColorFilter(Color.parseColor("#ff0000"))
            itemHolder.iconText.setTextColor(Color.parseColor("#ffffff"))
        }


        when (course.grade) {
            5.0 -> itemHolder.iconText.text = "A+"
            4.0 -> itemHolder.iconText.text = "A"
            3.7 -> itemHolder.iconText.text = "A-"
            3.3 -> itemHolder.iconText.text = "B+"
            3.0 -> itemHolder.iconText.text = "B"
            2.7 -> itemHolder.iconText.text = "B-"
            2.3 -> itemHolder.iconText.text = "C+"
            2.0 -> itemHolder.iconText.text = "C"
            1.7 -> itemHolder.iconText.text = "C-"
            1.3 -> itemHolder.iconText.text = "D+"
            1.0 -> itemHolder.iconText.text = "D"
            0.7 -> itemHolder.iconText.text = "D-"
            0.0 -> itemHolder.iconText.text = "F"
            0.1 -> itemHolder.iconText.text = "WF"
            -1.0 -> itemHolder.iconText.text = "W"
            else -> itemHolder.iconText.text = "N/A"

        }
    }

    override fun getHeaderViewHolder(view: View): RecyclerView.ViewHolder {
        return HeaderViewHolder(view)
    }

    override fun onBindHeaderViewHolder(holder: RecyclerView.ViewHolder) {
        val headerHolder: HeaderViewHolder = holder as HeaderViewHolder
        headerHolder.tvTitle.text = title
        headerHolder.imgArrow.setImageResource(
            if (isExpanded) R.drawable.ic_arrow_up else R.drawable.ic_arrow_down
        )
        headerHolder.rootView.setOnClickListener { clickListener.onHeaderRootViewClicked(this) }
    }

    internal interface ClickListener {
        fun onHeaderRootViewClicked(section: TranscriptSection)
        fun onItemRootViewClicked(
            sectionTitle: String,
            itemAdapterPosition: Int
        )
    }

}
