
package com.walentsoftware.schoolcompanion.data

import android.view.View
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.afollestad.recyclical.ViewHolder
import com.walentsoftware.schoolcompanion.R.id

class SemesterViewHolder(itemView: View) : ViewHolder(itemView) {
  val icon: ImageView = itemView.findViewById(id.icon)
  val title: TextView = itemView.findViewById(id.title)
  val body: TextView = itemView.findViewById(id.body)
}

class CourseViewHolder(itemView: View) : ViewHolder(itemView) {
  val icon: ImageView = itemView.findViewById(id.icon)
  val courseName: TextView = itemView.findViewById(id.courseName)
  val semesterName: TextView = itemView.findViewById(id.semesterName)
  val time: TextView = itemView.findViewById(id.courseTime)
  val hours: TextView = itemView.findViewById(id.hours)
  val completed: TextView = itemView.findViewById(id.completed)
}

class CompletedAssignmentViewHolder(itemView: View) : ViewHolder(itemView) {
  val icon: ImageView = itemView.findViewById(id.icon)
  val title: TextView = itemView.findViewById(id.title)
  val body: TextView = itemView.findViewById(id.body)
}

class TranscriptViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

    val courseName: TextView = itemView.findViewById(id.courseName)
    val credits: TextView = itemView.findViewById(id.credits)
    val icon: ImageView = itemView.findViewById(id.icon_profile)
    val iconText: TextView = itemView.findViewById(id.icon_text)
    val container: LinearLayout = itemView.findViewById(id.course_container)

}
