package com.walentsoftware.schoolcompanion.data

data class SemesterListItem(
    val id: String,
    val name: String,
    val completed: Boolean,
    val startDate: Double?,
    val endDate: Double?,
    val timestamp: Long?
) {
  companion object {
    @JvmStatic fun areTheSame(
      left: SemesterListItem,
      right: SemesterListItem
    ): Boolean {
      return left.id == right.id
    }

    @JvmStatic fun areContentsTheSame(
      left: SemesterListItem,
      right: SemesterListItem
    ): Boolean {
      return left.id == right.id &&
          left.name == right.name &&
          left.completed == right.completed &&
              left.startDate == right.startDate &&
              left.endDate == right.endDate &&
              left.timestamp == right.timestamp
    }
  }
}

data class CourseListItem(
    val id: String,
    val name:String,
    val completed: Boolean,
    val startTime: Long?,
    val endTime: Long?,
    val timestamp: Long,
    val hours: Int,
    val grade: Double?,
    val semesterID: String?,
    val colorOne: String,
    val colorTwo: String,
    var semesterName: String?,
    var semesterTimestamp: Int?

) {
    companion object {
        init {
            this.toString()
        }
    }
}


data class CompletedAssignmentListItem(
  val id: String,
  val name:String,
  val gained: Double?,
  val worth: Double?,
  val timestamp: Double
) {
  companion object {
    @JvmStatic fun areTheSame(
      left: CompletedAssignmentListItem,
      right: CompletedAssignmentListItem
    ): Boolean {
      return left.id == right.id
    }

    @JvmStatic fun areContentsTheSame(
      left: CompletedAssignmentListItem,
      right: CompletedAssignmentListItem
    ): Boolean {
      return left.id == right.id &&
              left.name == right.name &&
              left.worth == right.worth &&
              left.gained == right.gained &&
              left.timestamp == right.timestamp
    }
  }
}

data class TranscriptListItem(
    val semesterID: String,
    val semesterName: String,
    val timestamp: Long?,
    val section: Int,
    var courses: ArrayList<CourseListItem>?
) {
    companion object {
        @JvmStatic
        fun areTheSame(
            left: SemesterListItem,
            right: SemesterListItem
        ): Boolean {
            return left.id == right.id
        }

        @JvmStatic
        fun areContentsTheSame(
            left: SemesterListItem,
            right: SemesterListItem
        ): Boolean {
            return left.id == right.id &&
                    left.name == right.name &&
                    left.completed == right.completed &&
                    left.startDate == right.startDate &&
                    left.endDate == right.endDate &&
                    left.timestamp == right.timestamp
        }
    }
}
