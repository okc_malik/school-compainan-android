package com.walentsoftware.schoolcompanion.data.semester

data class SemesterData(val id: String, val name:String, val completed: Boolean, val startDate: Double?, val endDate: Double?, val timestamp: Double) {

}