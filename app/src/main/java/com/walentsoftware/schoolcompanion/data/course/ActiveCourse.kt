package com.walentsoftware.schoolcompanion.data.course

data class ActiveCourse(val courseID: String?, val courseName: String?, val colorOne: String?, val colorTwo: String? ) {

}