package com.walentsoftware.schoolcompanion.activities.home.ui.agenda

import android.graphics.Color
import androidx.lifecycle.LiveData
import com.walentsoftware.schoolcompanion.model.data.Block
import com.walentsoftware.schoolcompanion.model.data.CourseUpcomingAssignment
import java.util.*

interface AgendaRepository {
    fun getAgenda(): List<Block>

    fun getAssignments(): List<CourseUpcomingAssignment>

    fun getObservableAgenda(): LiveData<List<Block>>
}

class DefaultAgendaRepository
    : AgendaRepository {

    private val blocks by lazy {
        generateBlocks()
    }
    private val assignmnts by lazy {
        generateAssignments()
    }

    /**
     * Generates a list of [Block]s. Default values of each [Block] is supplied from the
     * default values stored as shared/src/main/res/xml/remote_config_defaults.xml.
     * Add a corresponding entry in RemoteConfig is any [Block]s need to be overridden.
     *
     */

    fun generateAssignments(): List<CourseUpcomingAssignment> {
        return listOf(
            CourseUpcomingAssignment(
                id = "asdfasdfsed",
                name = "Test Assignment 1",
                priority = 3,
                desc = "",
                isDark = true,
                dueDate = 1587301201000,
                color = 0xff5bb975.toInt(),
                strokeColor = Color.parseColor("#7fc8a5").toInt(),
                timestamp = 3
            ),
            CourseUpcomingAssignment(
                id = "asjlkfjioawx",
                name = "Test Assignment 2",
                priority = 1,
                desc = "Testing descritiond",
                dueDate = 1587301201000,
                color = 0xfffbbc05.toInt(),
                strokeColor = Color.parseColor("#fcc937"),
                timestamp = 2
            ),
            CourseUpcomingAssignment(
                id = "bbbbb",
                name = "Test Assignment 3",
                priority = 2,
                isDark = true,
                desc = "Testing descritiond",
                dueDate = 1587387601000,
                color = Color.parseColor("#fc3737"),
                strokeColor = Color.parseColor("#fd6969"),
                timestamp = 11
            )
        )
    }


    val calender = Calendar.getInstance()
    fun generateBlocks(): List<Block> {
        return listOf(
            Block(
                title = "Badge pick-up",
                type = "badge",
                color = 0xffe6e6e6.toInt(),
                startTime = calender

            ),
            Block(
                title = "Badge pick-up",
                type = "badge",
                color = 0xffe6e6e6.toInt(),
                startTime = calender

            ),
            Block(
                title = "Breakfast",
                type = "meal",
                color = 0xfffad2ce.toInt(),
                startTime = calender
            ),
            Block(
                title = "Google Keynote",
                type = "keynote",
                color = 0xfffbbc05.toInt(),
                startTime = Calendar.getInstance()
            ),
            Block(
                title = "I/O Store",
                type = "store",
                color = 0xffffffff.toInt(),
                strokeColor = 0xffff6c00.toInt(),
                startTime = Calendar.getInstance()
            ),
            Block(
                title = "Lunch",
                type = "meal",
                color = 0xfffad2ce.toInt(),
                startTime = Calendar.getInstance()
            ),
            Block(
                title = "Developer Keynote",
                type = "keynote",
                color = 0xfffbbc05.toInt(),
                startTime = Calendar.getInstance()
            ),
            Block(
                title = "Sessions",
                type = "session",
                isDark = true,
                color = 0xff5bb975.toInt(),
                startTime = Calendar.getInstance()
            ),
            Block(
                title = "Codelabs",
                type = "codelab",
                color = 0xff4285f4.toInt(),
                isDark = true,
                startTime = Calendar.getInstance()
            ),
            Block(
                title = "Office Hours & App Reviews",
                type = "office_hours",
                color = 0xff4285f4.toInt(),
                isDark = true,
                startTime = Calendar.getInstance()
            ),
            Block(
                title = "Sandboxes",
                type = "sandbox",
                color = 0xff4285f4.toInt(),
                isDark = true,
                startTime = Calendar.getInstance()
            ),
            Block(
                title = "After Dark",
                type = "after_hours",
                color = 0xff164fa5.toInt(),
                isDark = true,
                startTime = Calendar.getInstance()
            ),
            Block(
                title = "Badge pick-up",
                type = "badge",
                color = 0xffe6e6e6.toInt(),
                startTime = Calendar.getInstance()
            ),
            Block(
                title = "Breakfast",
                type = "meal",
                color = 0xfffad2ce.toInt(),
                startTime = Calendar.getInstance()
            ),
            Block(
                title = "I/O Store",
                type = "store",
                color = 0xffffffff.toInt(),
                strokeColor = 0xffff6c00.toInt(),
                startTime = Calendar.getInstance()
            ),
            Block(
                title = "Lunch",
                type = "meal",
                color = 0xfffad2ce.toInt(),
                startTime = Calendar.getInstance()
            ),
            Block(
                title = "Sessions",
                type = "session",
                isDark = true,
                color = 0xff5bb975.toInt(),
                startTime = Calendar.getInstance()
            ),
            Block(
                title = "Codelabs",
                type = "codelab",
                color = 0xff4285f4.toInt(),
                isDark = true,
                startTime = Calendar.getInstance()
            ),
            Block(
                title = "Office Hours & App Reviews",
                type = "office_hours",
                color = 0xff4285f4.toInt(),
                isDark = true,
                startTime = Calendar.getInstance()
            ),
            Block(
                title = "Sandboxes",
                type = "sandbox",
                color = 0xff4285f4.toInt(),
                isDark = true,
                startTime = Calendar.getInstance()
            ),
            Block(
                title = "Concert",
                type = "concert",
                color = 0xff164fa5.toInt(),
                isDark = true,
                startTime = Calendar.getInstance()
            ),
            Block(
                title = "Badge pick-up",
                type = "badge",
                color = 0xffe6e6e6.toInt(),
                startTime = Calendar.getInstance()
            ),
            Block(
                title = "Breakfast",
                type = "meal",
                color = 0xfffad2ce.toInt(),
                startTime = Calendar.getInstance()
            ),
            Block(
                title = "I/O Store",
                type = "store",
                color = 0xffffffff.toInt(),
                strokeColor = 0xffff6c00.toInt(),
                startTime = Calendar.getInstance()
            ),
            Block(
                title = "Lunch",
                type = "meal",
                color = 0xfffad2ce.toInt(),
                startTime = Calendar.getInstance()
            ),
            Block(
                title = "Sessions",
                type = "session",
                isDark = true,
                color = 0xff5bb975.toInt(),
                startTime = Calendar.getInstance()
            ),
            Block(
                title = "Codelabs",
                type = "codelab",
                color = 0xff4285f4.toInt(),
                isDark = true,
                startTime = Calendar.getInstance()
            ),
            Block(
                title = "Office Hours & App Reviews",
                type = "office_hours",
                color = 0xff4285f4.toInt(),
                isDark = true,
                startTime = Calendar.getInstance()
            ),
            Block(
                title = "Sandboxes",
                type = "sandbox",
                color = 0xff4285f4.toInt(),
                isDark = true,
                startTime = Calendar.getInstance()
            )
        )
    }

    override fun getAgenda(): List<Block> = blocks
    override fun getAssignments(): List<CourseUpcomingAssignment> = assignmnts

    override fun getObservableAgenda(): LiveData<List<Block>> {
        TODO("Not yet implemented")
    }


}
