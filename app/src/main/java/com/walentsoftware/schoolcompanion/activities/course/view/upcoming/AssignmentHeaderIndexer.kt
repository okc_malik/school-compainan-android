package com.walentsoftware.schoolcompanion.activities.course.view.upcoming

import com.walentsoftware.schoolcompanion.model.data.CourseUpcomingAssignment

fun indexAssignmentHeaders(assignmentItems: List<CourseUpcomingAssignment>): List<Pair<Int, Long>> {
    return assignmentItems
        .mapIndexed { index, block -> index to block.dueDate }
        .distinctBy { (it.second) }
}
