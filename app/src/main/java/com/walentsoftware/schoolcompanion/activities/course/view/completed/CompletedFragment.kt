package com.walentsoftware.schoolcompanion.activities.course.view.completed

import android.annotation.SuppressLint
import android.app.SearchManager
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.*
import android.widget.Filter
import android.widget.Filterable
import android.widget.TextView
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.SearchView
import androidx.core.widget.ContentLoadingProgressBar
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.RecyclerView
import com.afollestad.recyclical.datasource.SelectableDataSource
import com.afollestad.recyclical.datasource.emptySelectableDataSource
import com.afollestad.recyclical.itemdefinition.onChildViewClick
import com.afollestad.recyclical.setup
import com.afollestad.recyclical.swipe.SwipeLocation
import com.afollestad.recyclical.swipe.withSwipeAction
import com.afollestad.recyclical.viewholder.SelectionStateProvider
import com.afollestad.recyclical.viewholder.isSelected
import com.afollestad.recyclical.withItem
import com.google.android.material.snackbar.Snackbar
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.Query
import com.walentsoftware.schoolcompanion.R
import com.walentsoftware.schoolcompanion.activities.assignment.AssignmentDetailActivity
import com.walentsoftware.schoolcompanion.data.CompletedAssignmentListItem
import com.walentsoftware.schoolcompanion.data.CompletedAssignmentViewHolder
import com.walentsoftware.schoolcompanion.helper.PrimaryActionModeCallback
import com.walentsoftware.schoolcompanion.util.toast
import kotlinx.android.synthetic.main.fragment_completed.*
import java.util.*
import kotlin.collections.ArrayList


@SuppressLint("SetTextI18n")
class CompletedFragment : Fragment(), Filterable {

    private lateinit var auth: FirebaseAuth
    private lateinit var db: FirebaseFirestore
    private lateinit var emptyView: TextView
    private lateinit var courseID: String
    private lateinit var recyclerView: RecyclerView
    private var list = ArrayList<CompletedAssignmentListItem>()
    private lateinit var compFilteredList: List<CompletedAssignmentListItem>
    private var snackbar: Snackbar? = null



    private lateinit var progressBar: ContentLoadingProgressBar


    private val dataSource: SelectableDataSource<Any> = emptySelectableDataSource()
        .apply {
            onSelectionChange { invalidateCab() }
        }

    private var actionMode = PrimaryActionModeCallback()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        setHasOptionsMenu(true)
        courseID = arguments?.getString("courseID").toString()
        return inflater.inflate(R.layout.fragment_completed, container, false)
    }


    override fun onViewCreated(view: View, savedInstanceBundle: Bundle?) {
        super.onViewCreated(view, savedInstanceBundle)

        auth = FirebaseAuth.getInstance()

        db = FirebaseFirestore.getInstance()

        progressBar = view.findViewById(R.id.progress_circular)

        emptyView = view.findViewById(R.id.emptyView)
        recyclerView = view.findViewById(R.id.completedAssignmentList)

        actionMode.onActionItemClickListener = ::onActionModeItemClicked
        actionMode.onActionModeFinishedListener = ::finishActionMode


        if(courseID.isEmpty()) {

            progressBar.hide()
            emptyView.text = "Error fetching assignments"
        } else {
            fetchAssignments()
        }

        recyclerView.setup {
            withSwipeAction(SwipeLocation.LEFT) {
                icon(R.drawable.ic_action_delete)
                text(R.string.delete)
                color(R.color.md_red)
                callback { _, item ->
                    toast("Delete: $item")
                    true
                }
                //hapticFeedbackEnabled()
            }
            withSwipeAction(SwipeLocation.RIGHT) {
                icon(R.drawable.ic_action_archive)
                text(R.string.archive)
                color(R.color.md_green)
                callback { _, item ->
                    toast("Archive: $item")
                    true
                }
            }

            withEmptyView(emptyView)
            withDataSource(dataSource)

            withItem<CompletedAssignmentListItem, CompletedAssignmentViewHolder>(R.layout.completed_assignment_list_item) {
                //hasStableIds { it.timestamp }
                onBind(::CompletedAssignmentViewHolder) { _, item -> bindMyListItem(item) }

                onChildViewClick(CompletedAssignmentViewHolder::icon) { _, _ ->
                    toast("Clicked icon of ${item.name}!")
                    toggleSelection()
                }

                onClick { clickMyListItem() }
                onLongClick { toggleSelection() }
            }
        }


        snackbar = Snackbar.make(container, "Awaiting user action.", Snackbar.LENGTH_LONG)
    }

    private fun CompletedAssignmentViewHolder.bindMyListItem(item: CompletedAssignmentListItem) {
        if (isSelected()) {
            icon.visibility = View.VISIBLE
        } else {
            icon.visibility = View.GONE
        }
        icon.setImageResource(
            if (isSelected()) {
                R.drawable.ic_check_circle
            } else {
                R.drawable.open_circle
            }
        )
        title.text = item.name
        body.text = "Grade: ${item.gained}/${item.worth}"

    }

    private fun SelectionStateProvider<CompletedAssignmentListItem>.clickMyListItem() {
        if (hasSelection()) {
            // If we are in selection mode, click should toggle selection
            toggleSelection()
        } else {
            // Else just show a toast
            val intent = Intent(activity, AssignmentDetailActivity::class.java)
            intent.putExtra("ID", item.id)
            startActivity(intent)
            //toast("Clicked $index: ${item.id} / ${item.name}")
        }
    }

    private fun invalidateCab() {
        if (dataSource.hasSelection()) {
            startActionMode()
            if (actionMode.isActive) {
                actionMode.selectedItemsCount = dataSource.getSelectionCount()
            } else {
                startActionMode()
                actionMode.selectedItemsCount = dataSource.getSelectionCount()
            }

        } else {

            finishActionMode()
            (activity as AppCompatActivity?)!!.supportActionBar!!.show()
        }
    }

    private fun startActionMode() {
        if (actionMode.isActive) return


        (activity as AppCompatActivity?)!!.supportActionBar!!.hide()
        actionMode.startActionMode(requireActivity(), R.menu.upcoming_menu_action_mode)
        //activity?.actionBar?.hide()
    }

    private fun finishActionMode() {
        if (!actionMode.isActive) return

        dataSource.deselectAll()
        (activity as AppCompatActivity?)!!.supportActionBar!!.show()
        actionMode.finishActionMode()

    }

    private fun onActionModeItemClicked(item: MenuItem) {
        when (item.itemId) {
            R.id.action_delete -> deleteAssignments()
        }
    }

    private fun deleteAssignments() {
        val dialogBuilder = AlertDialog.Builder(requireContext())

        // set message of alert dialog
        dialogBuilder.setMessage("Are you sure you want to delete the assignment(s)? This action cannot be undone.")
            // if the dialog is cancelable
            .setCancelable(false)
            // positive button text and action
            .setPositiveButton("OK") { _, _ ->
                confirmedDelete()
            }
            .setNegativeButton("Cancel") { _, _ ->
            }

        // create dialog box
        val alert = dialogBuilder.create()
        // set title for alert dialog box
        alert.setTitle("Confirm")
        // show alert dialog
        alert.show()


    }

    private fun confirmedDelete() {
        val batch = db.batch()



        for (i in (dataSource.getSelectedItems().size.minus(1)).downTo(0)) {
            val assignmentRef = db.collection("assignments").document(list[i].id)
            batch.delete(assignmentRef)
            // list.removeAt(i)
        }

        batch.commit()
            .addOnSuccessListener {
//                for (i in (selected.size.minus(1)).downTo(0)) {
//                    list.removeAt(i)
//                }
                Log.w(TAG, "deletion good")
            }
            .addOnFailureListener { e ->
                Log.w(TAG, e.toString())
                showSnackbar(e.toString())
            }

        //assignmentAdapter.notifyDataSetChanged()
    }

    private fun showSnackbar(text: CharSequence) {
        snackbar?.setText(text)
        snackbar!!.show()
    }

    fun onBackPressed() {
        if (dataSource.hasSelection()) {
            dataSource.deselectAll()
        } else {
            super.getActivity()?.onBackPressed()
        }
    }

    private fun fetchAssignments() {
        Log.w(TAG, "Course Id is. $courseID")
        val semesterQuery = db.collection("assignments")
            .whereEqualTo("userID", auth.currentUser?.uid)
            .whereEqualTo("courseID", courseID)
            .whereEqualTo("completed", true)
            .orderBy("timestamp", Query.Direction.DESCENDING)

        semesterQuery.addSnapshotListener { value, e ->

            if ((progressBar.visibility == View.INVISIBLE) || (progressBar.visibility == View.GONE))
                progressBar.show()

            if (e != null) {
                Log.w(TAG, "Listen failed.", e)
                emptyView.text = e.toString()
                emptyView.visibility = View.VISIBLE
                compFilteredList = list
                return@addSnapshotListener
            }

            dataSource.clear()

            if(value!!.isEmpty) {
                progressBar.hide()
            } else {
                //emptyView.visibility = View.GONE
                dataSource.clear()
                list.clear()

                for (doc in value) {

                    dataSource.add(
                        CompletedAssignmentListItem(
                            id = doc.getString("assignmentID")!!,
                            name = doc.getString("assignmentName")!!,
                            gained = doc.getDouble("pointsGained"),
                            worth = doc.getDouble("assignmentWorth"),
                            timestamp = doc.getDouble("timestamp")!!
                        )
                    )
                    list.add(
                        CompletedAssignmentListItem(
                            id = doc.getString("assignmentID")!!,
                            name = doc.getString("assignmentName")!!,
                            gained = doc.getDouble("pointsGained"),
                            worth = doc.getDouble("assignmentWorth"),
                            timestamp = doc.getDouble("timestamp")!!
                        )
                    )
                }
                compFilteredList = list
                progressBar.hide()
            }
        }
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        // Do something that differs the Activity's menu here
        inflater.inflate(R.menu.fragment_completed, menu)

        // Associate searchable configuration with the SearchView
        val searchManager =
            requireActivity().getSystemService(Context.SEARCH_SERVICE) as SearchManager
        val searchView = menu.findItem(R.id.action_search)
            .actionView as SearchView
        searchView.setSearchableInfo(
            searchManager.getSearchableInfo(requireActivity().componentName)
        )
        searchView.maxWidth = Int.MAX_VALUE

        // listening to search query text change
        searchView.setOnQueryTextListener(object :
            SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String): Boolean {
                // filter recycler view when query submitted
                filter?.filter(query)
                return false
            }

            override fun onQueryTextChange(query: String): Boolean {
                // filter recycler view when text is changed
                filter?.filter(query)
                return false
            }
        })

    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.action_search -> {
            }
            R.id.action_info ->{
//                val intent = Intent(activity, CreateCourseActivity::class.java)
//                startActivity(intent)
                return true
            }
            R.id.action_delete -> {
                dataSource.toggleSelectionAt(0)
                return true
            }
        }
        return super.onOptionsItemSelected(item)
    }

    override fun getFilter(): Filter? {
        return object : Filter() {
            override fun performFiltering(charSequence: CharSequence): FilterResults {
                val charString = charSequence.toString()
                if (charString.isEmpty()) {
                    compFilteredList = list

                    requireActivity().runOnUiThread {
                        dataSource.clear()
                        dataSource.addAll(list)
                    }

                } else {
                    val filteredList: MutableList<CompletedAssignmentListItem> = ArrayList()
                    for (row in list) {

                        if (row.name.toLowerCase(Locale.ROOT)
                                .contains(charString.toLowerCase(Locale.ROOT))
                        ) {
                            filteredList.add(row)
                        }
                    }
                    compFilteredList = filteredList
                }
                val filterResults = FilterResults()
                filterResults.values = compFilteredList
                return filterResults
            }

            override fun publishResults(
                charSequence: CharSequence,
                filterResults: FilterResults
            ) {
                compFilteredList = filterResults.values as ArrayList<CompletedAssignmentListItem>
                requireActivity().runOnUiThread {

                    dataSource.clear()
                    dataSource.addAll(compFilteredList)
                }

            }
        }
    }

    companion object {

        private const val TAG = "CompletedFragment"


    }
}