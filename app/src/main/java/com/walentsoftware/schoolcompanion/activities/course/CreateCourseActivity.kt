package com.walentsoftware.schoolcompanion.activities.course

import android.app.AlertDialog
import android.app.TimePickerDialog
import android.app.TimePickerDialog.OnTimeSetListener
import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import android.text.TextUtils
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.lifecycle.Observer
import com.flask.colorpicker.ColorPickerView
import com.flask.colorpicker.builder.ColorPickerDialogBuilder
import com.google.android.material.card.MaterialCardView
import com.google.android.material.chip.Chip
import com.google.android.material.chip.ChipGroup
import com.google.android.material.snackbar.Snackbar
import com.google.android.material.switchmaterial.SwitchMaterial
import com.google.android.material.textfield.TextInputLayout
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.Query
import com.walentsoftware.schoolcompanion.App
import com.walentsoftware.schoolcompanion.R
import com.walentsoftware.schoolcompanion.activities.splash.LoginActivity
import com.walentsoftware.schoolcompanion.data.SemesterListItem
import com.walentsoftware.schoolcompanion.extension.bind
import kotlinx.android.synthetic.main.activity_create_course.*
import kotlinx.android.synthetic.main.content_create_course.*
import java.util.*
import kotlin.collections.ArrayList

class CreateCourseActivity : AppCompatActivity(), View.OnClickListener {

    private lateinit var auth: FirebaseAuth
    private lateinit var db: FirebaseFirestore
    private lateinit var semesterChipGroup: ChipGroup
    private lateinit var gradeChipGroup: ChipGroup
    private lateinit var emptyView: TextView
    private lateinit var courseNameTextView: TextInputLayout
    private lateinit var hoursTextView: TextInputLayout
    private lateinit var completedSwitch: SwitchMaterial
    private lateinit var startTimeValue: TextView
    private lateinit var endTimeValue: TextView
    private lateinit var colorOneCard: MaterialCardView
    private lateinit var colorTwoCard: MaterialCardView
    private lateinit var startTimeContainer: ConstraintLayout
    private lateinit var endTimeContainer: ConstraintLayout
    private lateinit var colorOneContainer: ConstraintLayout
    private lateinit var colorTwoContainer: ConstraintLayout
    private lateinit var gradeHeader: TextView
    private var snackbar: Snackbar? = null

    private var startTime: Long? = null
    private var endTime: Long? = null
    private var colorOne: String = "606c88"
    private var colorTwo: String = "3f4c6b"
    private var hr = 0
    private var min = 0
    private val grades =
        arrayOf("A+", "A", "A-", "B+", "B", "B-", "C+", "C", "C-", "D+", "D", "D-", "F", "W", "WF")
    private val semesters = ArrayList<SemesterListItem>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_create_course)

        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowHomeEnabled(true)

        val contextView = findViewById<View>(R.id.coordinator)
        snackbar = Snackbar.make(contextView, "Awaiting user action.", Snackbar.LENGTH_LONG)

        (application as App).preferenceRepository
            .nightModeLive.observe(this, Observer { nightMode ->
                nightMode?.let { delegate.localNightMode = it }
            }
            )

        auth = FirebaseAuth.getInstance()
        db = FirebaseFirestore.getInstance()


        semesterChipGroup = bind(R.id.semesterChipGroup)
        gradeChipGroup = bind(R.id.gradeChipGroup)
        emptyView = bind(R.id.emptyView)
        courseNameTextView = bind(R.id.courseNameTextView)
        hoursTextView = bind(R.id.hoursTextView)
        completedSwitch = bind(R.id.completedSwitch)
        startTimeValue = bind(R.id.startTimeValue)
        endTimeValue = bind(R.id.endTimeValue)
        colorOneCard = bind(R.id.colorOne)
        colorTwoCard = bind(R.id.colorTwo)
        startTimeContainer = bind(R.id.startTimeContainer)
        endTimeContainer = bind(R.id.endTimeContainer)
        colorOneContainer = bind(R.id.colorOneContainer)
        colorTwoContainer = bind(R.id.colorTwoContainer)
        gradeHeader = bind(R.id.headerFour)

        startTimeContainer.setOnClickListener(this)
        endTimeContainer.setOnClickListener(this)
        colorOneContainer.setOnClickListener(this)
        colorTwoContainer.setOnClickListener(this)

        completedSwitch.setOnCheckedChangeListener { _, checked ->
            if (checked) {
                gradeChipGroup.visibility = View.VISIBLE
                headerFour.visibility = View.VISIBLE
            } else {
                gradeChipGroup.visibility = View.GONE
                headerFour.visibility = View.GONE
            }
        }

        completedSwitch.isChecked = true

        colorOneCard.setBackgroundColor(Color.parseColor("#$colorOne"))
        colorTwoCard.setBackgroundColor(Color.parseColor("#$colorTwo"))

        semesterChipGroup.isSingleSelection = true
        gradeChipGroup.isSingleSelection = true

        initChipGroup(gradeChipGroup)



        fetchSemesters()

    }

    public override fun onStart() {
        super.onStart()
        // Check if user is signed in (non-null) and update UI accordingly.
        val currentUser = auth.currentUser

        if (currentUser == null) {

            val intent = Intent(this, LoginActivity::class.java)
            startActivity(intent)
            finish()
        }
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.done, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.action_done -> {
                createCourse()
                return true
            }

        }
        return super.onOptionsItemSelected(item)
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }

    private fun createCourse() {
        if (!validateForm()) {
            return
        }
        //showDialog()

        val newCourseRef = db.collection("courses").document()

        var gradeDouble: Double? = null
        if (completedSwitch.isChecked) {

            gradeDouble = getGradeDouble(grades[gradeChipGroup.checkedChipId])
        }

        val docData = hashMapOf(
            "courseID" to newCourseRef.id,
            "courseName" to courseNameTextView.editText?.text.toString(),
            "semesterID" to semesters[semesterChipGroup.checkedChipId].id,
            "userID" to auth.uid,
            "hours" to Integer.parseInt(hoursTextView.editText?.text.toString()),
            "completed" to completedSwitch.isChecked,
            "startTime" to startTime,
            "endTime" to endTime,
            "colorOne" to colorOne,
            "colorTwo" to colorTwo,
            "timestamp" to (Date().time / 1000),
            "grade" to gradeDouble
        )
        newCourseRef
            .set(docData)
            .addOnSuccessListener {
                Log.d(TAG, "DocumentSnapshot written with ID: ${newCourseRef.id}")
                //hideDialog()
                finish()
            }
            .addOnFailureListener { e ->
                Log.w(TAG, "Error adding document", e)
                //hideDialog()
                val builder = AlertDialog.Builder(this)
                //set title for alert dialog
                builder.setTitle("Error")
                //set message for alert dialog
                builder.setMessage(e.toString())
                builder.setIcon(android.R.drawable.ic_dialog_alert)

                //performing positive action
                builder.setPositiveButton("Okay") { dialogInterface, _ ->
                    dialogInterface.dismiss()
                }
            }
    }

    private fun validateForm(): Boolean {
        var valid = true

        val email = courseNameTextView.editText?.text.toString()
        if (TextUtils.isEmpty(email)) {
            courseNameTextView.error = "Required."
            valid = false
        } else {
            courseNameTextView.error = null
        }

        val password = hoursTextView.editText?.text.toString()
        if (TextUtils.isEmpty(password)) {
            hoursTextView.error = "Required."
            valid = false
        } else {
            if (!isInteger(password)) {
                valid = false
                hoursTextView.error = "Must be a number."
            } else {
                if (Integer.parseInt(password) <= 0) {
                    valid = false
                    hoursTextView.error = "Must be greater than 0."
                } else {
                    hoursTextView.error = null
                }
            }
        }

        val semesterID: Int? = semesterChipGroup.checkedChipId

        if (semesterID == null || semesterID < 0) {
            valid = false
            snackbar?.setText("Please choose a semester.")
            snackbar!!.show()
        }

        if (completedSwitch.isChecked) {
            val grade: Int? = gradeChipGroup.checkedChipId
            println(
                grade
            )
            if (grade == null || grade < 0) {
                valid = false
                snackbar?.setText("Please choose a grade or set the course to incomplete.")
                snackbar!!.show()
            }
        }

        println(
            semesterID
        )

        return valid
    }

    private fun isInteger(s: String): Boolean {
        try {
            s.toInt()
        } catch (e: NumberFormatException) {
            return false
        } catch (e: NullPointerException) {
            return false
        }
        // only got here if we didn't return false
        return true
    }

    private fun getGradeDouble(grade: String): Double {

        when (grade) {
            "A+" -> return 5.0
            "A" -> return 4.0
            "A-" -> return 3.7
            "B+" -> return 3.3
            "B" -> return 3.0
            "B-" -> return 2.7
            "C+" -> return 2.3
            "C" -> return 2.0
            "C-" -> return 1.7
            "D+" -> return 1.3
            "D" -> return 1.0
            "D-" -> return 0.7
            "F" -> return 0.0
            "WF" -> return 0.1
            "W" -> return -1.0
            else -> println("no match")
        }
        return -2.0
    }


    private fun initChipGroup(chipGroup: ChipGroup) {
        chipGroup.removeAllViews()
        for ((position, grade) in grades.withIndex()) {
            val chip = layoutInflater.inflate(
                R.layout.chip_choice,
                chipGroup,
                false
            ) as Chip
            chip.text = grade
            chip.id = position
            chip.isCloseIconVisible = false
            chip.setOnCloseIconClickListener {
                chipGroup.removeView(
                    chip
                )
            }
            chipGroup.addView(chip)
        }
    }

    private fun initSemesterChipGroup(chipGroup: ChipGroup) {
        chipGroup.removeAllViews()
        for ((position, semester) in semesters.withIndex()) {

            val chip = layoutInflater.inflate(
                R.layout.chip_choice,
                chipGroup,
                false
            ) as Chip
            chip.text = semester.name
            chip.id = position
            chip.isCloseIconVisible = false
            chip.setOnCloseIconClickListener {
                chipGroup.removeView(
                    chip
                )
            }
            chipGroup.addView(chip)
        }
    }

    private fun fetchSemesters() {
        val semesterQuery = db.collection("semesters")
            .whereEqualTo("userID", auth.currentUser?.uid)
            .orderBy("timestamp", Query.Direction.DESCENDING)

        semesterQuery.addSnapshotListener { value, e ->
            if (e != null) {
                Log.w(TAG, "Listen failed.", e)
                return@addSnapshotListener
            }

            if (value!!.isEmpty) {
                emptyView.visibility = View.VISIBLE
            } else {
                emptyView.visibility = View.GONE


                for (doc in value) {

                    semesters.add(
                        SemesterListItem(
                            id = doc.getString("semesterID")!!,
                            name = doc.getString("semesterName")!!,
                            completed = doc.getBoolean("completed")!!,
                            startDate = doc.getDouble("startDate"),
                            endDate = doc.getDouble("endDate"),
                            timestamp = doc.getLong("timestamp")!!
                        )
                    )
                }
                initSemesterChipGroup(semesterChipGroup)
            }
        }
    }

    override fun onClick(v: View) {
        when (v.id) {
            R.id.colorOneContainer -> {
                ColorPickerDialogBuilder
                    .with(this)
                    .setTitle("Pick A Color")
                    .initialColor(Color.parseColor("#$colorOne"))
                    .wheelType(ColorPickerView.WHEEL_TYPE.FLOWER)
                    .density(12)
                    .setOnColorChangedListener { selectedColor -> // Handle on color change
                        Log.d(
                            "ColorPicker",
                            "onColorChanged: 0x" + Integer.toHexString(selectedColor)
                        )
                    }
                    .setOnColorSelectedListener { selectedColor ->
//                        toast(
//                            "onColorSelected: 0x" + Integer.toHexString(
//                                selectedColor
//                            )
//                        )
                    }
                    .setPositiveButton(
                        "OK"
                    ) { _, selectedColor, _ ->
                        colorOneCard.setBackgroundColor(selectedColor)
                        colorOne = Integer.toHexString(selectedColor).toUpperCase(Locale.ROOT)
                        colorOne = colorOne.substring(2)
                        println(colorOne)
                    }
                    .setNegativeButton(
                        "cancel"
                    ) { _, _ -> }
                    .showColorEdit(true)
                    .showAlphaSlider(false)
                    .build()
                    .show()
            }
            R.id.colorTwoContainer -> {
                ColorPickerDialogBuilder
                    .with(this)
                    .setTitle("Pick A Color")
                    .initialColor(Color.parseColor("#$colorOne"))
                    .wheelType(ColorPickerView.WHEEL_TYPE.FLOWER)
                    .density(12)
                    .setOnColorChangedListener { selectedColor -> // Handle on color change
                        Log.d(
                            "ColorPicker",
                            "onColorChanged: 0x" + Integer.toHexString(selectedColor)
                        )
                    }
                    .setOnColorSelectedListener { selectedColor ->
//                        toast(
//                            "onColorSelected: 0x" + Integer.toHexString(
//                                selectedColor
//                            )
//                        )
                    }
                    .setPositiveButton(
                        "OK"
                    ) { _, selectedColor, _ ->
                        colorTwoCard.setBackgroundColor(selectedColor)
                        colorTwo = Integer.toHexString(selectedColor).toUpperCase(Locale.ROOT)
                        colorTwo = colorTwo.substring(2)
                        println(colorTwo)
                    }
                    .setNegativeButton(
                        "cancel"
                    ) { _, _ -> }
                    .showColorEdit(true)
                    .showAlphaSlider(false)
                    .build()
                    .show()
            }
            R.id.startTimeContainer -> {
                TimePickerDialog(this, startTimePickerListener, hr, min, false).show()
            }
            R.id.endTimeContainer -> {
                TimePickerDialog(this, endTimePickerListener, hr, min, false).show()
            }
        }
    }

    private val startTimePickerListener =
        OnTimeSetListener { _, hourOfDay, minutes ->
            hr = hourOfDay
            min = minutes
            updateTime(hr, min, true)
        }

    private val endTimePickerListener =
        OnTimeSetListener { _, hourOfDay, minutes ->
            hr = hourOfDay
            min = minutes
            updateTime(hr, min, false)
        }

    private fun updateTime(hourss: Int, mins: Int, start: Boolean) {
        var hours = hourss
        val timeSet: String
        when {
            hours > 12 -> {
                hours -= 12
                timeSet = "PM"
            }
            hours == 0 -> {
                hours += 12
                timeSet = "AM"
            }
            hours == 12 -> timeSet = "PM"
            else -> timeSet = "AM"
        }

        val minutes = if (mins < 10) "0$mins" else mins.toString()
        val aTime =
            StringBuilder().append(hours).append(':').append(minutes).append(" ")
                .append(timeSet).toString()

        val calendar: Calendar = Calendar.getInstance()
        calendar.set(Calendar.HOUR_OF_DAY, hours)
        calendar.set(Calendar.MINUTE, mins)
        calendar.set(Calendar.SECOND, 0)
        calendar.set(Calendar.MILLISECOND, 0)
        calendar.set(Calendar.HOUR, hourss)
        calendar.timeZone = TimeZone.getTimeZone("UTC")

        val sec = calendar.timeInMillis

        if (start) {
            startTime = sec
            startTimeValue.text = aTime
        } else {
            endTime = sec
            endTimeValue.text = aTime
        }

    }

    companion object {
        private const val TAG = "Create Course"
    }
}
