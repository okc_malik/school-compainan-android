package com.walentsoftware.schoolcompanion.activities.assignment

import android.annotation.SuppressLint
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.content.res.AppCompatResources
import androidx.lifecycle.Observer
import com.google.android.material.bottomappbar.BottomAppBar
import com.google.android.material.bottomappbar.BottomAppBarTopEdgeTreatment
import com.google.android.material.shape.CutCornerTreatment
import com.google.android.material.shape.MaterialShapeDrawable
import com.google.android.material.snackbar.Snackbar
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.DocumentReference
import com.google.firebase.firestore.FirebaseFirestore
import com.walentsoftware.schoolcompanion.App
import com.walentsoftware.schoolcompanion.R
import com.walentsoftware.schoolcompanion.activities.splash.LoginActivity
import com.walentsoftware.schoolcompanion.helper.BottomAppBarCutCornersTopEdge
import kotlinx.android.synthetic.main.activity_assignment_detail.*
import kotlinx.android.synthetic.main.content_assignment_detail.*
import java.text.DecimalFormat
import java.text.SimpleDateFormat
import java.util.*

@SuppressLint("SetTextI18n")
class AssignmentDetailActivity : AppCompatActivity() {

    private var assignmentID: String? = null
    private lateinit var auth: FirebaseAuth
    private lateinit var db: FirebaseFirestore
    private var snackbar: Snackbar? = null
    private var completed = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_assignment_detail)
        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowHomeEnabled(true)

        assignmentID = intent.getStringExtra("ID")

        auth = FirebaseAuth.getInstance()
        db = FirebaseFirestore.getInstance()




        snackbar = Snackbar.make(coordinator, "Awaiting user action.", Snackbar.LENGTH_LONG)


        fab!!.setOnClickListener {
            updateAssignmentStatus(!completed)
        }


        bar?.fabAlignmentMode = BottomAppBar.FAB_ALIGNMENT_MODE_END
        bar?.hideOnScroll = true

        bar.inflateMenu(R.menu.assignment_detial)
        bar.setOnMenuItemClickListener {
            when (it.itemId) {
                R.id.action_delete -> confirmDeletion()
                R.id.action_edit -> edit()
                else -> showSnackbar("Unknown option")
            }
            true
        }

        setUpBottomAppBarShapeAppearance()



        (application as App).preferenceRepository
            .nightModeLive.observe(this, Observer { nightMode ->
                nightMode?.let { delegate.localNightMode = it }
            }
            )

    }

    override fun onResume() {
        super.onResume()
        if (assignmentID.isNullOrBlank()) {
            progressCircular.hide()
            infoTextView.text = "Error Retrieving Assignment"
            fab?.isEnabled = false
        } else
            fetchAssignmentDetail()
    }

    public override fun onStart() {
        super.onStart()
        // Check if user is signed in (non-null) and update UI accordingly.
        val currentUser = auth.currentUser

        if (currentUser == null) {

            val intent = Intent(this, LoginActivity::class.java)
            startActivity(intent)
            finish()
        }
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }

    private fun setUpBottomAppBarShapeAppearance() {
        val fabShapeAppearanceModel = fab!!.shapeAppearanceModel
        val cutCornersFab =
            (fabShapeAppearanceModel.bottomLeftCorner is CutCornerTreatment
                    && fabShapeAppearanceModel.bottomRightCorner is CutCornerTreatment)
        val topEdge =
            if (cutCornersFab) BottomAppBarCutCornersTopEdge(
                bar!!.fabCradleMargin,
                bar!!.fabCradleRoundedCornerRadius,
                bar!!.cradleVerticalOffset
            ) else BottomAppBarTopEdgeTreatment(
                bar!!.fabCradleMargin,
                bar!!.fabCradleRoundedCornerRadius,
                bar!!.cradleVerticalOffset
            )
        val babBackground = bar!!.background as MaterialShapeDrawable
        babBackground.shapeAppearanceModel =
            babBackground.shapeAppearanceModel.toBuilder().setTopEdge(topEdge).build()

    }

    private fun confirmDeletion() {
        val dialogBuilder = AlertDialog.Builder(this)

        // set message of alert dialog
        dialogBuilder.setMessage("Are you sure you want to delete this assignment, this cannot be undone?")
            // if the dialog is cancelable
            .setCancelable(true)
            // positive button text and action
            .setPositiveButton("Delete") { _, _ -> deleteAssignment() }
            // negative button text and action
            .setNegativeButton("Cancel") { dialog, _ -> dialog.cancel() }

        // create dialog box
        val alert = dialogBuilder.create()
        // set title for alert dialog box
        alert.setTitle("Confirm")
        // show alert dialog
        alert.show()
    }

    private fun deleteAssignment() {

        db.collection("assignments").document(assignmentID!!)
            .delete()
            .addOnSuccessListener {
                Log.d(TAG, "DocumentSnapshot successfully deleted!")
                finish()
            }
            .addOnFailureListener { e ->
                Log.w(TAG, "Error deleting document", e)
                showSnackbar(e.toString())
            }

    }

    private fun edit() {
        val intent = Intent(this, EditAssignmentActivity::class.java)
        intent.putExtra("assignmentID", assignmentID)
        startActivity(intent)
    }

    private fun fetchAssignmentDetail() {

        val docRef: DocumentReference = db.collection("assignments").document(assignmentID!!)
        docRef.get()
            .addOnSuccessListener { document ->
                if (document != null) {
                    Log.d(TAG, "DocumentSnapshot data: ${document.data}")


                    updateValues(
                        document.getString("assignmentName")!!,
                        document.getLong("priority")!!.toInt(),
                        document.getString("notes"),
                        document.getBoolean("completed")!!,
                        document.getDouble("pointsGained")!!,
                        document.getDouble("assignmentWorth")!!,
                        document.getLong("dueDate")
                    )
                } else {
                    Log.d(TAG, "No such document")
                    progressCircular.hide()
                    infoTextView.text = "No such document."
                }
            }
            .addOnFailureListener { exception ->
                Log.d(TAG, "get failed with ", exception)
                progressCircular.hide()
                infoTextView.text = exception.toString()
            }

    }


    @SuppressLint("SimpleDateFormat")
    private fun updateValues(
        assignmentName: String,
        priority: Int,
        desc: String?,
        completed: Boolean,
        gained: Double,
        worth: Double,
        dueDate: Long?
    ) {
        this.assignmentName.text = assignmentName

        if (desc.isNullOrBlank())
            assignmentDescription.text = "No Notes."
        else
            assignmentDescription.text = desc



        when (priority) {
            0 -> {
                this.assignmentPriority.text = "• Low Priority"
                assignmentPriorityIcon.setImageDrawable(
                    AppCompatResources.getDrawable(
                        assignmentPriorityIcon.context,
                        R.drawable.ic_priority_low
                    )
                )
            }
            1 -> {
                this.assignmentPriority.text = "• Medium Priority"
                assignmentPriorityIcon.setImageDrawable(
                    AppCompatResources.getDrawable(
                        assignmentPriorityIcon.context,
                        R.drawable.ic_priority_med
                    )
                )
            }
            2 -> {
                this.assignmentPriority.text = "• High Priority"
                assignmentPriorityIcon.setImageDrawable(
                    AppCompatResources.getDrawable(
                        assignmentPriorityIcon.context,
                        R.drawable.ic_priority_high
                    )
                )
            }
            else -> this.assignmentPriority.text = "• No Priority"
        }

        this.completed = completed

        when (completed) {
            true -> {
                assignmentStatus.text = "• Completed"
            }
            else -> assignmentStatus.text = "• Incomplete"

        }

        val df = DecimalFormat("0.00")

        if (worth < 1)
            assignmentGrade.text = "• $gained / $worth (${df.format((gained / gained) * 100)}%"
        else
            assignmentGrade.text = "• $gained / $worth (${df.format((gained / worth) * 100)}%"

        if (dueDate != null) {
            val cal = Calendar.getInstance(TimeZone.getTimeZone("UTC"))
            cal.timeInMillis = dueDate
            val format = SimpleDateFormat("EEEE, MMMM d, yyyy")

            this.assignmentDueDate.text = format.format(cal.time)
        } else
            this.assignmentDueDate.text = "No Due Date"

        progressCircular.hide()
        infoTextView.visibility = View.GONE

    }

    private fun updateAssignmentStatus(completed: Boolean) {
        progressCircular.show()
        val assignmentRef = db.collection("assignments").document(assignmentID!!)

        assignmentRef
            .update(
                "completed", completed
            )
            .addOnSuccessListener {
                Log.d(TAG, "DocumentSnapshot successfully updated!")
                progressCircular.hide()
                when (completed) {
                    true -> {
                        assignmentStatus.text = "• Completed"
                        showSnackbar("Assignment Completed")
                    }
                    else -> {
                        assignmentStatus.text = "• Incomplete"
                        showSnackbar("Assignment Incomplete")
                    }
                }

            }
            .addOnFailureListener { e ->
                progressCircular.hide()

                Log.w(TAG, "Error adding document", e)
                showSnackbar(e.toString())
            }


    }

    companion object {
        private const val TAG = "Create Assignment"
    }

    private fun showSnackbar(text: CharSequence) {
        snackbar?.setText(text)
        snackbar!!.show()
    }
}