/*
 * Copyright 2018 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.walentsoftware.schoolcompanion.activities.home.ui.agenda

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.walentsoftware.schoolcompanion.R
import com.walentsoftware.schoolcompanion.interfaces.CourseUpcomingAssignmentListener
import com.walentsoftware.schoolcompanion.model.data.Block


class AgendaAdapter(
    private val listener: CourseUpcomingAssignmentListener,
    private val list: List<Block>
) : RecyclerView.Adapter<AgendaViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): AgendaViewHolder {


        return AgendaViewHolder(
            DataBindingUtil.inflate(LayoutInflater.from(parent.context), viewType, parent, false)

        )
    }

    override fun onBindViewHolder(holder: AgendaViewHolder, position: Int) {
        holder.bind(list[position])
        applyClickEvents(holder, position)
    }

    override fun getItemViewType(position: Int): Int {
        return if (list[position].isDark) {
            R.layout.item_agenda_dark
        } else {
            R.layout.item_agenda_light
        }
    }

    private fun applyClickEvents(holder: AgendaViewHolder, position: Int) {

        holder.itemView.findViewById<View>(R.id.backgroundView).setOnClickListener {

            listener.onRowClicked(
                position
            )
        }
    }

    override fun getItemCount(): Int {
        return list.size
    }

}

class AgendaViewHolder(
    private val binding: ViewDataBinding
) : RecyclerView.ViewHolder(binding.root) {
    fun bind(block: Block) {
        binding.setVariable(1, block)
        binding.executePendingBindings()
    }
}

object BlockDiff : DiffUtil.ItemCallback<Block>() {
    override fun areItemsTheSame(oldItem: Block, newItem: Block): Boolean {
        return oldItem.title == newItem.title &&
                oldItem.startTime == newItem.startTime
    }

    override fun areContentsTheSame(oldItem: Block, newItem: Block) = oldItem == newItem
}
