package com.walentsoftware.schoolcompanion.activities.home.ui.course

import android.annotation.SuppressLint
import android.app.SearchManager
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.*
import android.widget.Filter
import android.widget.Filterable
import android.widget.TextView
import androidx.annotation.WorkerThread
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.SearchView
import androidx.core.widget.ContentLoadingProgressBar
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.recyclerview.widget.RecyclerView
import com.afollestad.recyclical.datasource.SelectableDataSource
import com.afollestad.recyclical.datasource.emptySelectableDataSource
import com.afollestad.recyclical.itemdefinition.onChildViewClick
import com.afollestad.recyclical.setup
import com.afollestad.recyclical.swipe.SwipeLocation
import com.afollestad.recyclical.swipe.withSwipeAction
import com.afollestad.recyclical.viewholder.SelectionStateProvider
import com.afollestad.recyclical.viewholder.isSelected
import com.afollestad.recyclical.withItem
import com.google.android.gms.tasks.Tasks
import com.google.android.material.snackbar.Snackbar
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.Query
import com.walentsoftware.schoolcompanion.R
import com.walentsoftware.schoolcompanion.activities.course.CreateCourseActivity
import com.walentsoftware.schoolcompanion.activities.course.view.ViewCourseActivity
import com.walentsoftware.schoolcompanion.data.CourseListItem
import com.walentsoftware.schoolcompanion.data.CourseViewHolder
import com.walentsoftware.schoolcompanion.helper.PrimaryActionModeCallback
import com.walentsoftware.schoolcompanion.util.toast
import kotlinx.android.synthetic.main.fragment_courses.*
import java.text.SimpleDateFormat
import java.util.*
import java.util.concurrent.Callable
import java.util.concurrent.LinkedBlockingQueue
import java.util.concurrent.ThreadPoolExecutor
import java.util.concurrent.TimeUnit
import kotlin.collections.ArrayList


class CoursesFragment : Fragment(), FragmentManager.OnBackStackChangedListener, Filterable {

    private lateinit var auth: FirebaseAuth
    private lateinit var db: FirebaseFirestore
    private lateinit var progressBar: ContentLoadingProgressBar
    private lateinit var emptyView: TextView
    private lateinit var recyclerView: RecyclerView
    private var list = ArrayList<CourseListItem>()
    private lateinit var compFilteredList: List<CourseListItem>
    private var snackbar: Snackbar? = null

    private var actionMode = PrimaryActionModeCallback()

    private lateinit var courses: ArrayList<CourseListItem>

    private val dataSource: SelectableDataSource<Any> = emptySelectableDataSource()
        .apply {
            onSelectionChange { invalidateCab() }
        }


    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        setHasOptionsMenu(true)
        return inflater.inflate(R.layout.fragment_courses, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        auth = FirebaseAuth.getInstance()

        db = FirebaseFirestore.getInstance()
        progressBar = view.findViewById(R.id.progress_circular)
        emptyView = view.findViewById(R.id.emptyView)

        actionMode.onActionItemClickListener = ::onActionModeItemClicked
        actionMode.onActionModeFinishedListener = ::finishActionMode
        snackbar = Snackbar.make(container, "Awaiting user action.", Snackbar.LENGTH_LONG)

        fetchCourses { fetchCourses ->
            println(fetchCourses)
            //fetchSemesters(it)

            courses = fetchCourses

            var count = 0

            for (course in courses) {
                if (course.semesterID != null) {

                    val docRef = db.collection("semesters").document(course.semesterID)
                    docRef.get()
                        .addOnSuccessListener { document ->
                            if (document != null) {
                                Log.d(TAG, "DocumentSnapshot data: ${document.data}")
                                course.semesterName = document.getString("semesterName")

                                course.semesterTimestamp = document.getLong("timestamp")?.toInt()
                                println("The course $course")
                                count++
                                println(count)

                                if (count > courses.size - 1)

                                    updateRecycler()

                            } else {
                                Log.d(TAG, "No such document")
                            }
                        }
                        .addOnFailureListener { exception ->
                            Log.d(TAG, "get failed with ", exception)
                        }
                }
            }

        }

        recyclerView = view.findViewById(R.id.courseList)

        recyclerView.setup {
            withSwipeAction(SwipeLocation.LEFT) {
                icon(R.drawable.ic_action_delete)
                text(R.string.delete)
                color(R.color.md_red)
                callback { _, item ->
                    toast("Delete: $item")
                    true
                }
                //hapticFeedbackEnabled()
            }
            withSwipeAction(SwipeLocation.RIGHT) {
                icon(R.drawable.ic_action_archive)
                text(R.string.archive)
                color(R.color.md_green)
                callback { _, item ->
                    toast("Archive: $item")
                    true
                }
            }

            withEmptyView(emptyView)
            withDataSource(dataSource)

            withItem<CourseListItem, CourseViewHolder>(R.layout.course_list_item) {
                onBind(::CourseViewHolder) { _, item -> bindMyListItem(item) }

                onChildViewClick(CourseViewHolder::icon) { _, _ ->
                    toast("Clicked icon of ${item.name}!")
                    toggleSelection()
                }

                onClick { clickMyListItem() }
                onLongClick { toggleSelection() }
            }
        }
    }

    override fun onBackStackChanged() {
        onBackPressed()
    }

    private fun onBackPressed() {
        if (dataSource.hasSelection()) {
            dataSource.deselectAll()
        } else {
            super.getActivity()?.onBackPressed()
        }
    }

    @SuppressLint("SetTextI18n")
    private fun CourseViewHolder.bindMyListItem(item: CourseListItem) {
        if (isSelected()) {
            icon.visibility = View.VISIBLE
        } else {
            icon.visibility = View.INVISIBLE
        }
        icon.setImageResource(
            if (isSelected()) {
                R.drawable.ic_check_circle
            } else {
                R.drawable.open_circle
            }
        )

        courseName.text = item.name
        hours.text = "${item.hours} hours"
        semesterName.text = item.semesterName
        if (item.completed) {
            completed.text = "Completed"
        } else {
            completed.text = "In Progress"
        }

        if ((item.startTime != null) && (item.endTime != null)) {
            val startTime = Date(item.startTime.toLong())
            val endTime = Date(item.endTime.toLong())
            val format = SimpleDateFormat("h:mm a", Locale.US)
            format.timeZone = TimeZone.getTimeZone("UTC")
            val startTimeString =  format.format(startTime)
            val endTimeString =  format.format(endTime)
            time.text = "$startTimeString - $endTimeString"
        } else {
            time.text = "No Time"
        }


    }

    private fun SelectionStateProvider<CourseListItem>.clickMyListItem() {
        if (hasSelection()) {
            // If we are in selection mode, click should toggle selection
            toggleSelection()
        } else {
            // Else just show a toast
            val intent = Intent(activity, ViewCourseActivity::class.java)
            intent.putExtra("courseID",item.id)
            intent.putExtra("courseName",item.name)
            startActivity(intent)
            //toast("Clicked $index: ${item.id} / ${item.name}")
        }
    }

    private fun invalidateCab() {
        if (dataSource.hasSelection()) {
            startActionMode()
            if (actionMode.isActive) {
                actionMode.selectedItemsCount = dataSource.getSelectionCount()
            } else {
                startActionMode()
                actionMode.selectedItemsCount = dataSource.getSelectionCount()
            }

        } else {

            finishActionMode()
            (activity as AppCompatActivity?)!!.supportActionBar!!.show()
        }
    }

    private fun startActionMode() {
        if (actionMode.isActive) return


        (activity as AppCompatActivity?)!!.supportActionBar!!.hide()
        actionMode.startActionMode(requireActivity(), R.menu.upcoming_menu_action_mode)
    }

    private fun finishActionMode() {
        if (!actionMode.isActive) return

        dataSource.deselectAll()
        (activity as AppCompatActivity?)!!.supportActionBar!!.show()
        actionMode.finishActionMode()

    }

    private fun onActionModeItemClicked(item: MenuItem) {
        when (item.itemId) {
            R.id.action_delete -> deleteAssignments()
        }
    }

    private fun deleteAssignments() {
        val dialogBuilder = AlertDialog.Builder(requireContext())

        // set message of alert dialog
        dialogBuilder.setMessage(
            "Are you sure you want to delete the course(s)? This action cannot be undone and the assignments for each" +
                    "course will be deleted."
        )
            // if the dialog is cancelable
            .setCancelable(false)
            // positive button text and action
            .setPositiveButton("OK") { _, _ ->
                confirmedDelete()
            }
            .setNegativeButton("Cancel") { _, _ ->
            }

        // create dialog box
        val alert = dialogBuilder.create()
        // set title for alert dialog box
        alert.setTitle("Confirm")
        // show alert dialog
        alert.show()


    }


    @WorkerThread
    @Throws(Exception::class)
    private fun confirmedDelete() {
        val batch = db.batch()
        val EXECUTOR = ThreadPoolExecutor(
            2, 4,
            60, TimeUnit.SECONDS, LinkedBlockingQueue()
        )

        val assignments = ArrayList<String>()
        var count = 0

        for (i in (dataSource.getSelectedItems().size.minus(1)).downTo(0)) {


            fetchAssignments(list[i].id) { fetchedAssignments ->
                assignments.addAll(fetchedAssignments)
                count++
                Log.d(TAG, "$count count is ")
                val courseRef = db.collection("courses").document(list[i].id)

                batch.delete(courseRef)

                if (count == dataSource.getSelectionCount()) {
                    for (assignment in assignments) {
                        val assignmentRef = db.collection("assignments").document(assignment)

                        batch.delete(assignmentRef)
                    }


                    Tasks.call(EXECUTOR, Callable<Void> {
                        batch.commit()
                            .addOnSuccessListener {
                                Log.w(TAG, "deletion good")
                            }
                            .addOnFailureListener { e ->
                                Log.w(TAG, e.toString())
                                showSnackbar(e.toString())
                            }
                        progressBar.hide()
                        null
                    })

                }
            }


        }


    }

    private fun fetchAssignments(courseID: String, callback: (ArrayList<String>) -> Unit) {
        if ((progressBar.visibility == View.INVISIBLE) || (progressBar.visibility == View.GONE))
            progressBar.show()
        val assignments = ArrayList<String>()
        db.collection("assignments").whereEqualTo("courseID", courseID)
            .get()
            .addOnSuccessListener { documents ->
                for (document in documents) {
                    Log.d(TAG, "${document.id} => ${document.data}")
                    assignments.add(document.id)
                }

                callback(assignments)
            }
            .addOnFailureListener { exception ->
                Log.w(TAG, "Error getting documents: ", exception)
                callback(assignments)
            }


    }


    private fun showSnackbar(text: CharSequence) {
        snackbar?.setText(text)
        snackbar!!.show()
    }

    private fun fetchCourses(callback: (ArrayList<CourseListItem>) -> Unit) {
        if ((progressBar.visibility == View.INVISIBLE) || (progressBar.visibility == View.GONE))
            progressBar.show()

        val semesterQuery = db.collection("courses")
            .whereEqualTo("userID", auth.currentUser?.uid)
            .orderBy("timestamp", Query.Direction.DESCENDING)

        semesterQuery.addSnapshotListener { value, e ->

            dataSource.clear()

            if (e != null) {
                Log.w(TAG, "Listen failed.", e)
                emptyView.text = e.toString()
                progressBar.hide()
                emptyView.visibility = View.VISIBLE
                dataSource.clear()

                return@addSnapshotListener
            }


            val courses: ArrayList<CourseListItem> = ArrayList()

            if(value!!.isEmpty) {
                progressBar.hide()
                dataSource.clear()
                compFilteredList = list
                emptyView.visibility = View.VISIBLE
            } else {
                //emptyView.visibility = View.GONE

                println(value.documents.toString())

                for (doc in value) {
                    val courseList = CourseListItem(
                        id = doc.getString("courseID")!!,
                        name = doc.getString("courseName")!!,
                        completed = doc.getBoolean("completed")!!,
                        startTime = doc.getLong("startTime"),
                        endTime = doc.getLong("endTime"),
                        timestamp = doc.getLong("timestamp")!!,
                        grade = doc.getDouble("grade"),
                        hours = doc.getLong("hours")!!.toInt(),
                        semesterID = doc.getString("semesterID"),
                        semesterName = "No Semester",
                        colorOne = doc.getString("colorOne")!!,
                        colorTwo = doc.getString("colorTwo")!!,
                        semesterTimestamp = null
                    )

                    courses.add(courseList)

                }
                callback(courses)
            }
        }
    }

    private fun updateRecycler() {

        courses.sortByDescending { it.timestamp }
        for (course in courses) {
            dataSource.add(course)
            list.add(
                course
            )
        }
        compFilteredList = list
        progressBar.hide()
        emptyView.visibility = View.GONE
    }


    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        // Do something that differs the Activity's menu here
        inflater.inflate(R.menu.create_delete, menu)

        val searchManager =
            requireActivity().getSystemService(Context.SEARCH_SERVICE) as SearchManager
        val searchView = menu.findItem(R.id.action_search)
            .actionView as SearchView
        searchView.setSearchableInfo(
            searchManager.getSearchableInfo(requireActivity().componentName)
        )
        searchView.maxWidth = Int.MAX_VALUE

        // listening to search query text change
        searchView.setOnQueryTextListener(object :
            SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String): Boolean {
                // filter recycler view when query submitted
                filter?.filter(query)
                return false
            }

            override fun onQueryTextChange(query: String): Boolean {
                // filter recycler view when text is changed
                filter?.filter(query)
                return false
            }
        })
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.action_create ->{
                val intent = Intent(activity, CreateCourseActivity::class.java)
                startActivity(intent)
                return true
            }
            R.id.action_delete -> {
                dataSource.toggleSelectionAt(0)
                return true
            }
            else -> {
            }
        }
        return super.onOptionsItemSelected(item)
    }

    override fun getFilter(): Filter? {
        return object : Filter() {
            override fun performFiltering(charSequence: CharSequence): FilterResults {
                val charString = charSequence.toString()
                if (charString.isEmpty()) {
                    compFilteredList = list

                    requireActivity().runOnUiThread {
                        dataSource.clear()
                        dataSource.addAll(list)
                    }

                } else {
                    val filteredList: MutableList<CourseListItem> = ArrayList()
                    for (row in list) {

                        if (row.name.toLowerCase(Locale.ROOT)
                                .contains(charString.toLowerCase(Locale.ROOT))
                        ) {
                            filteredList.add(row)
                        }
                    }
                    compFilteredList = filteredList
                }
                val filterResults = FilterResults()
                filterResults.values = compFilteredList
                return filterResults
            }

            override fun publishResults(
                charSequence: CharSequence,
                filterResults: FilterResults
            ) {
                compFilteredList = filterResults.values as ArrayList<CourseListItem>
                requireActivity().runOnUiThread {

                    dataSource.clear()
                    dataSource.addAll(compFilteredList)
                }

            }
        }
    }

    companion object {

        private const val TAG = "CoursesFragment"


    }
}