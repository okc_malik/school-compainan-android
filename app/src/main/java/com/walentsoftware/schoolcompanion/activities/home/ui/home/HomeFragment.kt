package com.walentsoftware.schoolcompanion.activities.home.ui.home


import android.app.Activity
import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import android.util.DisplayMetrics
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.core.graphics.ColorUtils
import androidx.core.widget.ContentLoadingProgressBar
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.android.material.bottomsheet.BottomSheetBehavior.BottomSheetCallback
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.Query
import com.walentsoftware.schoolcompanion.R
import com.walentsoftware.schoolcompanion.activities.assignment.AssignmentDetailActivity
import com.walentsoftware.schoolcompanion.activities.course.view.ViewCourseActivity
import com.walentsoftware.schoolcompanion.activities.course.view.upcoming.AssignmentHeadersDecoration
import com.walentsoftware.schoolcompanion.adapter.course.ActiveCourseAdapter
import com.walentsoftware.schoolcompanion.adapter.upcoming.CourseUpcomingAssignmentsAdapter
import com.walentsoftware.schoolcompanion.data.course.ActiveCourse
import com.walentsoftware.schoolcompanion.extension.clearDecorations
import com.walentsoftware.schoolcompanion.interfaces.ActiveCourseAdapterListener
import com.walentsoftware.schoolcompanion.interfaces.CourseUpcomingAssignmentListener
import com.walentsoftware.schoolcompanion.model.data.CourseUpcomingAssignment
import kotlinx.android.synthetic.main.fragment_home.*
import java.text.SimpleDateFormat
import java.util.*
import java.util.concurrent.TimeUnit
import kotlin.collections.ArrayList
import kotlin.math.abs


class HomeFragment : Fragment(),
    ActiveCourseAdapterListener, CourseUpcomingAssignmentListener {

    private lateinit var auth: FirebaseAuth
    private lateinit var db: FirebaseFirestore
    private lateinit var courses: ArrayList<ActiveCourse>
    private lateinit var courseAdapter: ActiveCourseAdapter
    private lateinit var assignmentAdapter: CourseUpcomingAssignmentsAdapter
    private val assignmentsList = ArrayList<CourseUpcomingAssignment>()
    private lateinit var courseRecyclerView: RecyclerView
    private lateinit var allUpcomingAssignmentsRecyclerView: RecyclerView
    private lateinit var emptyView: TextView
    private lateinit var assignmentEmptyView: TextView
    private lateinit var assignmentsDueValue: TextView
    private lateinit var progressBar: ContentLoadingProgressBar


    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        return inflater.inflate(R.layout.fragment_home, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        auth = FirebaseAuth.getInstance()

        db = FirebaseFirestore.getInstance()

        todaysDateValue.text = getDate()

        emptyView = view.findViewById(R.id.emptyView)
        assignmentEmptyView = view.findViewById(R.id.assignmentEmptyView)

        assignmentsDueValue = view.findViewById(R.id.assignmentsDueValue)

        progressBar = view.findViewById(R.id.progressBar)


        //Set up assignments bottom sheet
        val bottomSheetPersistent: View = view.findViewById(R.id.bottom_drawer)
        val params = bottomSheetPersistent.layoutParams
        val bottomSheetBehavior =
            BottomSheetBehavior.from(bottomSheetPersistent)
        val windowHeight = getWindowHeight()

        bottomSheetBehavior.setUpdateImportantForAccessibilityOnSiblings(true)
        bottomSheetBehavior.addBottomSheetCallback(createBottomSheetCallback())
        params.height = windowHeight
        bottomSheetPersistent.layoutParams = params
        bottomSheetBehavior.isFitToContents = false
        //bottomSheetBehavior.peekHeight = 200
        val alphaColor: Int = ColorUtils.setAlphaComponent( R.attr.colorSurface,  60)
        bottomSheetPersistent.setBackgroundColor(alphaColor)



        courseRecyclerView = view.findViewById(R.id.courseRecyclerView)
        allUpcomingAssignmentsRecyclerView =
            view.findViewById(R.id.allUpcomingAssignmentsRecyclerView)

        //fetchAssignmentCount()


        //Set Up RecyclerView
        val courseLayoutManager =
            LinearLayoutManager(activity, LinearLayoutManager.HORIZONTAL, false)

        courseRecyclerView.layoutManager = courseLayoutManager

        assignmentAdapter = CourseUpcomingAssignmentsAdapter(this, assignmentsList)

        if (allUpcomingAssignmentsRecyclerView.adapter == null) {
            allUpcomingAssignmentsRecyclerView.adapter = assignmentAdapter
        }

        fetchActiveCourses()
        fetchAssignments()
    }

    // Calculate window height for fullscreen use

    private fun getWindowHeight(): Int {
        val displayMetrics = DisplayMetrics()
        (context as Activity?)!!.windowManager.defaultDisplay
            .getMetrics(displayMetrics)
        return displayMetrics.heightPixels
    }


    private fun createBottomSheetCallback(): BottomSheetCallback { // Set up BottomSheetCallback
        return object : BottomSheetCallback() {
            override fun onStateChanged(bottomSheet: View, newState: Int) {
                when (newState) {
//                    BottomSheetBehavior.STATE_DRAGGING -> //ext.setText(R.string.cat_bottomsheet_state_dragging)
//                    BottomSheetBehavior.STATE_EXPANDED -> //text.setText(R.string.cat_bottomsheet_state_expanded)
//                    BottomSheetBehavior.STATE_COLLAPSED -> //text.setText(R.string.cat_bottomsheet_state_collapsed)
                    BottomSheetBehavior.STATE_HALF_EXPANDED -> {
                        val bottomSheetBehavior =
                            BottomSheetBehavior.from(bottomSheet)
                        bottomSheetBehavior.state = BottomSheetBehavior.STATE_COLLAPSED

                    }
                    else -> {
                    }
                }
            }

            override fun onSlide(
                bottomSheet: View,
                slideOffset: Float
            ) {
            }
        }
    }

    private fun getDate(): String  {

        val date = Calendar.getInstance().time
        return date.toString("EEEE, MMM dd, yyyy")

    }

    private fun Date.toString(format: String, locale: Locale = Locale.getDefault()): String {
        val formatter = SimpleDateFormat(format, locale)
        return formatter.format(this)
    }

    private fun fetchAssignmentCount() {
        val start = Calendar.getInstance()
        start[Calendar.HOUR] = 0
        start[Calendar.MINUTE] = 0
        start[Calendar.SECOND] = 0
        start[Calendar.HOUR_OF_DAY] = 0
        start[Calendar.MILLISECOND] = 0

        val end = Calendar.getInstance()
        end[Calendar.HOUR] = 11
        end[Calendar.MINUTE] = 59
        end[Calendar.SECOND] = 59
        end[Calendar.HOUR_OF_DAY] = 22
        end[Calendar.MILLISECOND] = 999

        val startEpoch = start.timeInMillis
        val endEpoch = end.timeInMillis

        println("the start epoch size is $startEpoch")

        val assignmentQuery = db.collection("assignments")
            .whereEqualTo("userID", auth.currentUser?.uid)
            .whereGreaterThanOrEqualTo("dueDate", startEpoch)
            .whereLessThanOrEqualTo("dueDate", endEpoch)
            .whereEqualTo("completed", false)

            assignmentQuery.addSnapshotListener { value, e ->
                if (e != null) {
                    Log.w(TAG, "Listen failed.", e)
                    return@addSnapshotListener
                }

                println("the value size is ${value!!.size()}")
                assignmentsDueValue.text = value.size().toString()

            }
    }

    private fun fetchActiveCourses() {
        val courseQuery = db.collection("courses")
            .whereEqualTo("userID", auth.currentUser?.uid)
            .whereEqualTo("completed", false)
            .orderBy("timestamp", Query.Direction.DESCENDING)

        courseQuery.addSnapshotListener { value, e ->
            if (e != null) {
                Log.w(TAG, "Listen failed.", e)
                return@addSnapshotListener
            }

            courses = ArrayList()
            courses.clear()

            if(value!!.isEmpty) {
                emptyView.visibility = View.VISIBLE
            } else {
                emptyView.visibility = View.GONE

                for (doc in value) {

                    println(doc.getString("colorOne"))
                    val course =
                        ActiveCourse(
                            doc.getString("courseID"),
                            doc.getString("courseName"),
                            doc.getString("colorOne"),
                            doc.getString("colorTwo")
                        )

                    courses.add(course)

                }
            }
            println(courses)
            courseAdapter =
                ActiveCourseAdapter(
                    courses,
                    this
                )
            courseRecyclerView.adapter = courseAdapter

        }
    }

    private fun fetchAssignments() {
        val assignmentsQuery = db.collection("assignments")
            .whereEqualTo("userID", auth.currentUser?.uid)
            .whereEqualTo("completed", false)
            .orderBy("dueDate", Query.Direction.ASCENDING)

        assignmentsQuery.addSnapshotListener { value, e ->

            if ((progressBar.visibility == View.INVISIBLE) || (progressBar.visibility == View.GONE))
                progressBar.show()

            if (e != null) {
                Log.w(TAG, "Listen failed.", e)
                assignmentEmptyView.text = e.toString()
                progressBar.hide()
                assignmentEmptyView.visibility = View.VISIBLE
                assignmentsList.clear()
                assignmentAdapter.notifyDataSetChanged()
                updateHeaders()
                return@addSnapshotListener
            }


            if (value!!.isEmpty) {
                progressBar.hide()
            } else {
                emptyView.visibility = View.GONE
                assignmentsList.clear()


                var count = 0

                for (doc in value) {

                    val description = doc.getString("notes")
                    var desc = doc.getString("notes")

                    if (description.isNullOrBlank() || description.isNullOrBlank()) {
                        desc = "No Notes"
                    }

                    var color: Int
                    var strokeColor: Int
                    var isDark = false


                    val priority = doc.getLong("priority")!!.toInt()


                    val todayDate = Calendar.getInstance(TimeZone.getTimeZone("UTC"))

                    val cal = Calendar.getInstance(TimeZone.getTimeZone("UTC"))
                    cal.timeInMillis = doc.getLong("dueDate")!!

                    if (calendarDaysBetween(todayDate, cal).toInt() == 0)
                        count++


                    when (priority) {
                        0 -> {
                            color = 0xff5bb975.toInt()
                            strokeColor = Color.parseColor("#7fc8a5")
                            isDark = true
                        }
                        1 -> {
                            color = 0xfffbbc05.toInt()
                            strokeColor = Color.parseColor("#fcc937")
                        }
                        2 -> {
                            color = Color.parseColor("#fc3737")
                            strokeColor = Color.parseColor("#fd6969")
                            isDark = true
                        }
                        else -> {
                            color = 0xff5bb975.toInt()
                            strokeColor = Color.parseColor("#7fc8a5")
                            isDark = true
                        }
                    }

                    assignmentsList.add(
                        CourseUpcomingAssignment(
                            id = doc.getString("assignmentID")!!,
                            name = doc.getString("assignmentName")!!,
                            dueDate = doc.getLong("dueDate")!!,
                            priority = priority,
                            desc = desc,
                            color = color,
                            isDark = isDark,
                            strokeColor = strokeColor,
                            timestamp = doc.getLong("timestamp")!!
                        )
                    )
                }
                assignmentAdapter.notifyDataSetChanged()
                updateHeaders()
                assignmentEmptyView.visibility = View.GONE
                progressBar.hide()

                assignmentsDueValue.text = count.toString()
            }
        }
    }

    private fun updateHeaders() {
        allUpcomingAssignmentsRecyclerView.clearDecorations()
        if (assignmentsList.isNotEmpty()) {
            allUpcomingAssignmentsRecyclerView.addItemDecoration(
                AssignmentHeadersDecoration(
                    allUpcomingAssignmentsRecyclerView.context,
                    assignmentsList
                )
            )
        }
    }
    companion object {

        private const val TAG = "HomeFragment"


    }

    override fun onCourseRowClicked(position: Int) {
        println("The course name is ${courses[position].courseName}")
        val intent = Intent(activity, ViewCourseActivity::class.java)
        intent.putExtra("courseID", courses[position].courseID)
        intent.putExtra("courseName", courses[position].courseName)
        startActivity(intent)
    }

    override fun onRowClicked(position: Int) {

        println("row $position was clicked")


        // read the message which removes bold from the row
        val intent = Intent(activity, AssignmentDetailActivity::class.java)
        intent.putExtra("ID", assignmentsList[position].id)
        startActivity(intent)

    }

    override fun onIconClicked(position: Int) {

    }

    override fun onLongRowClicked(position: Int) {

    }

    private fun calendarDaysBetween(startCal: Calendar, endCal: Calendar): Long {

        val start = Calendar.getInstance()
        start.timeZone = startCal.timeZone
        start.timeInMillis = startCal.timeInMillis
        val end = Calendar.getInstance()
        end.timeZone = endCal.timeZone
        end.timeInMillis = endCal.timeInMillis

        // Set the copies to be at midnight, but keep the day information.
        start[Calendar.HOUR_OF_DAY] = 0
        start[Calendar.MINUTE] = 0
        start[Calendar.SECOND] = 0
        start[Calendar.MILLISECOND] = 0
        end[Calendar.HOUR_OF_DAY] = 0
        end[Calendar.MINUTE] = 0
        end[Calendar.SECOND] = 0
        end[Calendar.MILLISECOND] = 0

        // At this point, each calendar is set to midnight on
        // their respective days. Now use TimeUnit.MILLISECONDS to
        // compute the number of full days between the two of them.
        return TimeUnit.MILLISECONDS.toDays(
            abs(end.timeInMillis - start.timeInMillis)
        )
    }


}
