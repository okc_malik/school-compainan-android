package com.walentsoftware.schoolcompanion.activities.home.ui.agenda

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.updatePaddingRelative
import androidx.databinding.BindingAdapter
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.RecyclerView
import com.walentsoftware.schoolcompanion.R
import com.walentsoftware.schoolcompanion.databinding.FragmentAgendaBinding
import com.walentsoftware.schoolcompanion.extension.clearDecorations
import com.walentsoftware.schoolcompanion.extension.doOnApplyWindowInsets
import com.walentsoftware.schoolcompanion.extension.viewModelProvider
import com.walentsoftware.schoolcompanion.interfaces.CourseUpcomingAssignmentListener
import com.walentsoftware.schoolcompanion.model.data.Block
import com.walentsoftware.schoolcompanion.model.view.agenda.AgendaViewModel
import com.walentsoftware.schoolcompanion.model.view.agenda.ScoreViewModelFactory

@BindingAdapter(value = ["agendaItems"])
fun agendaItems(recyclerView: RecyclerView, list: List<Block>?) {
    list ?: return

    println("agenda items are called")
//    if (recyclerView.adapter == null) {
//        recyclerView.adapter = AgendaAdapter(AgendaFragment())
//    }
//    (recyclerView.adapter as AgendaAdapter).apply {
//        println("the list for the adapter is :$list")
//        submitList(list)
//
//    }
//
//    // Recreate the decoration used for the sticky date headers
//    recyclerView.clearDecorations()
//    if (list.isNotEmpty()) {
//        recyclerView.addItemDecoration(
//            AgendaHeadersDecoration(recyclerView.context, list)
//        )
//    }
}

class AgendaFragment : Fragment(), CourseUpcomingAssignmentListener {


    private lateinit var viewModelFactory: ViewModelProvider.Factory
    private lateinit var viewModel: AgendaViewModel
    private lateinit var binding: FragmentAgendaBinding
    private lateinit var recyclerView: RecyclerView
    private var list: List<Block>? = null
    private lateinit var mAgendaAdapter: AgendaAdapter

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentAgendaBinding.inflate(inflater, container, false).apply {
            lifecycleOwner = viewLifecycleOwner
        }
        // Pad the bottom of the RecyclerView so that the content scrolls up above the nav bar
        binding.recyclerView.doOnApplyWindowInsets { v, insets, padding ->
            v.updatePaddingRelative(bottom = padding.bottom + insets.systemWindowInsetBottom)
        }
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        recyclerView = view.findViewById(R.id.recyclerView)
        list = DefaultAgendaRepository().generateBlocks()
        mAgendaAdapter = AgendaAdapter(this, list!!)

        if (recyclerView.adapter == null) {
            recyclerView.adapter = mAgendaAdapter
        }


        // Recreate the decoration used for the sticky date headers
        recyclerView.clearDecorations()
        if (list!!.isNotEmpty()) {
            recyclerView.addItemDecoration(
                AgendaHeadersDecoration(recyclerView.context, list!!)
            )
        }

    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        viewModelFactory =
            ScoreViewModelFactory()



        viewModel = viewModelProvider(viewModelFactory)

        binding.viewModel = viewModel

    }

    override fun onRowClicked(position: Int) {

        println(" the position is : $position")
    }

    override fun onIconClicked(position: Int) {
        TODO("Not yet implemented")
    }

    override fun onLongRowClicked(position: Int) {
        TODO("Not yet implemented")
    }


}






