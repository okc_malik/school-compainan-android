package com.walentsoftware.schoolcompanion.activities.home.ui.settings

import android.app.TimePickerDialog
import android.app.TimePickerDialog.OnTimeSetListener
import android.content.Intent
import android.content.pm.PackageInfo
import android.content.pm.PackageManager
import android.os.Bundle
import android.text.format.DateFormat
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AlertDialog
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore
import com.walentsoftware.schoolcompanion.App
import com.walentsoftware.schoolcompanion.R
import com.walentsoftware.schoolcompanion.activities.splash.LoginActivity
import kotlinx.android.synthetic.main.content_settings.*
import java.util.*


class SettingsFragment : Fragment(), View.OnClickListener {

    private lateinit var auth: FirebaseAuth
    private lateinit var db: FirebaseFirestore
//    private lateinit var emailTextView: TextView
//    private lateinit var buildTextView: TextView
//    private lateinit var emailContainer: ConstraintLayout
//    private lateinit var passContainer: ConstraintLayout
//    private lateinit var logoutContainer: ConstraintLayout
//    private lateinit var notiContainer: ConstraintLayout
//    private lateinit var helpContainer: ConstraintLayout
//    private lateinit var rateContainer: ConstraintLayout
//    private lateinit var privContainer: ConstraintLayout

    private var hr = 11
    private var min = 0
    private var isTwentyFourClock = false

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_settings, container, false)
    }


    override fun onViewCreated(view: View, savedInstanceBundle: Bundle?) {
        super.onViewCreated(view, savedInstanceBundle)

        auth = FirebaseAuth.getInstance()
        db = FirebaseFirestore.getInstance()

        isTwentyFourClock = DateFormat.is24HourFormat(requireActivity())

        emailContainer.setOnClickListener(this)
        resetPassContainer.setOnClickListener(this)
        logoutContainer.setOnClickListener(this)
        notiContainer.setOnClickListener(this)
        helpContainer.setOnClickListener(this)
        rateContainer.setOnClickListener(this)
        privacyContainer.setOnClickListener(this)
        notiTimeContainer.setOnClickListener(this)

        emailTextView.text = auth.currentUser?.email

        try {
            val pInfo: PackageInfo =
                requireActivity().applicationContext.packageManager.getPackageInfo(
                    requireActivity().packageName,
                    0
                )
            versionNumber.text = pInfo.versionName
        } catch (e: PackageManager.NameNotFoundException) {
            e.printStackTrace()
        }

        val preferenceRepository = (requireActivity().application as App).preferenceRepository

        preferenceRepository.isDarkThemeLive.observe(requireActivity(), Observer { isDarkTheme ->
            isDarkTheme?.let { themeSwitch.isChecked = it }
        })

        themeSwitch.setOnCheckedChangeListener { _, checked ->
            preferenceRepository.isDarkTheme = checked
        }

    }

    override fun onClick(v: View) {
        when (v.id) {
            R.id.emailContainer -> {
                val intent = Intent(activity, EditEmailActivity::class.java)
                startActivity(intent)
            }
            R.id.resetPassContainer -> {
                val user = auth.currentUser

                if (user != null) {
                    user.email?.let {
                        auth.sendPasswordResetEmail(it)
                            .addOnCompleteListener { task ->
                                if (task.isSuccessful) {
                                    Log.d("Settings Activity", "Email sent.")
                                    val dialogBuilder = AlertDialog.Builder(requireContext())

                                    // set message of alert dialog
                                    dialogBuilder.setMessage("A password reset email has successfully been sent.")
                                        // if the dialog is cancelable
                                        .setCancelable(false)
                                        // positive button text and action
                                        .setPositiveButton("OK") { _, _ ->
                                        }

                                    // create dialog box
                                    val alert = dialogBuilder.create()
                                    // set title for alert dialog box
                                    alert.setTitle("Success")
                                    // show alert dialog
                                    alert.show()
                                }
                            }
                    }
                }
            }
            R.id.logoutContainer -> {
                val dialogBuilder = AlertDialog.Builder(requireContext())

                // set message of alert dialog
                dialogBuilder.setMessage("Are you sure you want to sign out?")
                    // if the dialog is cancelable
                    .setCancelable(false)
                    // positive button text and action
                    .setPositiveButton("OK") { _, _ ->
                        FirebaseAuth.getInstance().signOut()

                        val intent = Intent(activity, LoginActivity::class.java)
                        startActivity(intent)
                        requireActivity().finish()
                    }
                    .setNegativeButton("Cancel") { _, _ ->
                    }

                // create dialog box
                val alert = dialogBuilder.create()
                // set title for alert dialog box
                alert.setTitle("Confirm")
                // show alert dialog
                alert.show()

            }
            R.id.notiContainer -> {
                val intent = Intent()
                intent.action = "android.settings.APP_NOTIFICATION_SETTINGS"

                intent.putExtra("app_package", requireActivity().packageName)
                intent.putExtra("app_uid", requireActivity().applicationInfo.uid)

                intent.putExtra("android.provider.extra.APP_PACKAGE", requireActivity().packageName)

                startActivity(intent)
            }
            R.id.notiTimeContainer -> {
                TimePickerDialog(
                    requireActivity(),
                    timePickerListener,
                    hr,
                    min,
                    isTwentyFourClock
                ).show()
                //CustomTimePickerDialog(requireActivity(), timePickerListener, hr, min, isTwentyFourClock).show()
            }
        }
    }

    private val timePickerListener =
        OnTimeSetListener { _, hourOfDay, minutes ->
            hr = hourOfDay
            min = minutes
            updateTime(hr, min)
        }

    private fun updateTime(hourss: Int, mins: Int) {
        var hours = hourss
        var timeSet = ""

        if (!isTwentyFourClock) {
            when {
                hours > 12 -> {
                    hours -= 12
                    timeSet = "PM"
                }
                hours == 0 -> {
                    hours += 12
                    timeSet = "AM"
                }
                hours == 12 -> timeSet = "PM"
                else -> timeSet = "AM"
            }
        }


        val minutes = if (mins < 10) "0$mins" else mins.toString()
        val aTime =
            StringBuilder().append(hours).append(':').append(minutes).append(" ")
                .append(timeSet).toString()

        val calendar: Calendar = Calendar.getInstance()
        calendar.set(Calendar.HOUR_OF_DAY, hours)
        calendar.set(Calendar.MINUTE, mins)
        calendar.set(Calendar.SECOND, 0)
        calendar.set(Calendar.MILLISECOND, 0)
        calendar.set(Calendar.HOUR, hourss)
        calendar.timeZone = TimeZone.getTimeZone("UTC")

        val sec = calendar.timeInMillis
        val userRef = db.collection("users").document(auth.currentUser?.uid!!)
        userRef
            .update("notificationTime", sec)
            .addOnSuccessListener {
                Log.d(TAG, "Notification  time successfully updated!")
                notiTimeValue.text = aTime
            }
            .addOnFailureListener { e ->

                Log.w(TAG, "Error Notification  time  document", e)

            }

    }

    companion object {
        private const val TAG = "Settings"
    }
}