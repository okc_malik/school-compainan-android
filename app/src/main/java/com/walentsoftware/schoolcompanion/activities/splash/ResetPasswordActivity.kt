package com.walentsoftware.schoolcompanion.activities.splash

import android.content.Intent
import android.os.Bundle
import android.text.TextUtils
import android.util.Log
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import com.google.android.material.button.MaterialButton
import com.google.android.material.textfield.TextInputLayout
import com.google.firebase.auth.FirebaseAuth
import com.walentsoftware.schoolcompanion.App
import com.walentsoftware.schoolcompanion.R
import com.walentsoftware.schoolcompanion.activities.home.HomeActivity

class ResetPasswordActivity : AppCompatActivity() {

    private lateinit var auth: FirebaseAuth

    private var emailTextView: TextInputLayout? = null
    private var loginButton: MaterialButton? = null
    private var forgotPassButton: MaterialButton? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_reset_password)

        emailTextView = findViewById(R.id.emailTextView)
        loginButton = findViewById(R.id.loginButton)
        forgotPassButton = findViewById(R.id.forgotPassButton)

        auth = FirebaseAuth.getInstance()

        loginButton?.setOnClickListener {
            finish()
        }

        forgotPassButton?.setOnClickListener {
            reset(emailTextView?.editText?.text.toString())
        }

        (application as App).preferenceRepository
            .nightModeLive.observe(this, Observer { nightMode ->
                nightMode?.let { delegate.localNightMode = it }
            }
            )

    }

    public override fun onStart() {
        super.onStart()
        // Check if user is signed in (non-null) and update UI accordingly.
        val currentUser = auth.currentUser
        //updateUI(currentUser)

        if (currentUser != null) {

            val intent = Intent(this, HomeActivity::class.java)
            startActivity(intent)
            finish()
        }
    }

    private fun reset(email: String) {

        Log.d(TAG, "signIn:$email")
        if (!validateForm()) {

            return
        }

        // [START sign_in_with_email]
        auth.sendPasswordResetEmail(email)
            .addOnCompleteListener { task ->
                if (task.isSuccessful) {
                    Log.d(TAG, "Email sent.")
                    Toast.makeText(
                        applicationContext,
                        "Email Sent.",
                        Toast.LENGTH_SHORT
                    ).show()
                    finish()
                }
            }
    }

    private fun validateForm(): Boolean {
        var valid = true

        val email = emailTextView?.editText?.text.toString()
        if (TextUtils.isEmpty(email)) {
            emailTextView?.error = "Required."
            valid = false
        } else {
            emailTextView?.error = null
        }
        return valid
    }


    companion object {
        private const val TAG = "ResetActivity"
    }
}
