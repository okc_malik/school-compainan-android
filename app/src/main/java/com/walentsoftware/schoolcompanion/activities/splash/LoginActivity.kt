package com.walentsoftware.schoolcompanion.activities.splash


import android.content.Intent
import android.os.Bundle
import android.text.TextUtils
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import com.google.android.material.button.MaterialButton
import com.google.android.material.snackbar.Snackbar
import com.google.android.material.textfield.TextInputLayout
import com.google.firebase.auth.FirebaseAuth
import com.walentsoftware.schoolcompanion.App
import com.walentsoftware.schoolcompanion.R
import com.walentsoftware.schoolcompanion.activities.home.HomeActivity
import kotlinx.android.synthetic.main.activity_login.*

class LoginActivity : AppCompatActivity() {

    private lateinit var auth: FirebaseAuth

    private var emailTextView : TextInputLayout? = null
    private var passwordTextView : TextInputLayout? = null
    private var loginButton : MaterialButton? = null
    private var registerButton : MaterialButton? = null
    private var forgotPassButton : MaterialButton? = null
    private var snackbar: Snackbar? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        emailTextView = findViewById(R.id.emailTextView)
        passwordTextView = findViewById(R.id.passwordTextField)
        loginButton = findViewById(R.id.loginButton)
        registerButton = findViewById(R.id.createAccountButton)
        forgotPassButton = findViewById(R.id.forgotPassButton)

        auth = FirebaseAuth.getInstance()

        snackbar = Snackbar.make(coordinator, "Awaiting user action.", Snackbar.LENGTH_LONG)

        loginButton?.setOnClickListener {
            // your code to perform when the user clicks on the button

            signIn(emailTextView?.editText?.text.toString(), passwordTextView?.editText?.text.toString())
        }

        registerButton?.setOnClickListener {
            val intent = Intent(this, RegisterActivity::class.java)
            startActivity(intent)
        }

        forgotPassButton?.setOnClickListener {
            val intent = Intent(this, ResetPasswordActivity::class.java)
            startActivity(intent)
        }

        (application as App).preferenceRepository
            .nightModeLive.observe(this, Observer { nightMode ->
                nightMode?.let { delegate.localNightMode = it }
            }
            )

    }

    public override fun onStart() {
        super.onStart()
        // Check if user is signed in (non-null) and update UI accordingly.
        val currentUser = auth.currentUser
        //updateUI(currentUser)

        if (currentUser != null) {

            val intent = Intent(this, HomeActivity::class.java)
            startActivity(intent)
            finish()
        }
    }

    private fun signIn(email: String, password: String) {

        Log.d(TAG, "signIn:$email")
        if (!validateForm()) {

            return
        }

        // [START sign_in_with_email]
        auth.signInWithEmailAndPassword(email, password)
            .addOnCompleteListener(this) { task ->
                if (task.isSuccessful) {
                    // Sign in success, update UI with the signed-in user's information
                    Log.d(TAG, "signInWithEmail:success")
                    //val user = auth.currentUser
                    val intent = Intent(this, HomeActivity::class.java)
                    startActivity(intent)
                } else {
                    // If sign in fails, display a message to the user.
                    Log.w(TAG, "signInWithEmail:failure", task.exception)
                    showSnackbar("Authentication failed.")
                }

            }
    }

    private fun validateForm(): Boolean {
        var valid = true

        val email = emailTextView?.editText?.text.toString()
        if (TextUtils.isEmpty(email)) {
            emailTextView?.error = "Required."
            valid = false
        } else {
            emailTextView?.error = null
        }

        val password = passwordTextView?.editText?.text.toString()
        if (TextUtils.isEmpty(password)) {
            passwordTextView?.error = "Required."
            valid = false
        } else {
            passwordTextView?.error = null
        }

        return valid
    }

    private fun showSnackbar(text: CharSequence) {
        snackbar?.setText(text)
        snackbar!!.show()
    }

    companion object {
        private const val TAG = "LoginActivity"
    }
}
