package com.walentsoftware.schoolcompanion.activities.home.ui.transcript

import android.annotation.SuppressLint
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import android.widget.Toast
import androidx.core.widget.ContentLoadingProgressBar
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.Query
import com.walentsoftware.schoolcompanion.R
import com.walentsoftware.schoolcompanion.data.CourseListItem
import com.walentsoftware.schoolcompanion.data.TranscriptListItem
import com.walentsoftware.schoolcompanion.data.sections.TranscriptSection
import io.github.luizgrp.sectionedrecyclerviewadapter.SectionAdapter
import io.github.luizgrp.sectionedrecyclerviewadapter.SectionedRecyclerViewAdapter
import java.text.DecimalFormat

@SuppressLint("SetTextI18n")
class TranscriptFragment : Fragment(), TranscriptSection.ClickListener {

    private var sectionedAdapter: SectionedRecyclerViewAdapter? = null

    private lateinit var auth: FirebaseAuth
    private lateinit var db: FirebaseFirestore
    private lateinit var transcriptList: ArrayList<TranscriptListItem>
    private val hoursList = ArrayList<Int>()
    private val gradesList = ArrayList<Double>()

    private lateinit var progressBar: ContentLoadingProgressBar
    private lateinit var infoText: TextView
    private lateinit var gpaText: TextView

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_transcript, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceBundle: Bundle?) {
        super.onViewCreated(view, savedInstanceBundle)

        auth = FirebaseAuth.getInstance()

        db = FirebaseFirestore.getInstance()

        sectionedAdapter = SectionedRecyclerViewAdapter()


        progressBar = view.findViewById(R.id.progressBar)
        infoText = view.findViewById(R.id.infoText)
        gpaText = view.findViewById(R.id.gpaText)


        val recyclerView: RecyclerView = view.findViewById(R.id.upcomingAssignmentList)
        recyclerView.layoutManager = LinearLayoutManager(context)
        recyclerView.adapter = sectionedAdapter

        fetchTranscript { semesters ->
            println(semesters)
            //fetchSemesters(it)

            transcriptList = semesters

            var count = 0

            for (semester in transcriptList) {
                db.collection("courses")
                    .whereEqualTo("semesterID", semester.semesterID)
                    .whereEqualTo("completed", true)
                    .get()
                    .addOnSuccessListener { documents ->
                        val courses = ArrayList<CourseListItem>()
                        for (document in documents) {
                            Log.d(TAG, "${document.id} => ${document.data}")
                            gradesList.add(document.getDouble("grade")!!)
                            hoursList.add(document.getLong("hours")!!.toInt())
                            val course = CourseListItem(
                                id = document.getString("courseID")!!,
                                name = document.getString("courseName")!!,
                                completed = document.getBoolean("completed")!!,
                                startTime = document.getLong("startTime"),
                                endTime = document.getLong("endTime"),
                                timestamp = document.getLong("timestamp")!!,
                                grade = document.getDouble("grade"),
                                hours = document.getLong("hours")!!.toInt(),
                                semesterID = document.getString("semesterID"),
                                semesterName = "No Semester",
                                colorOne = document.getString("colorOne")!!,
                                colorTwo = document.getString("colorTwo")!!,
                                semesterTimestamp = null
                            )

                            courses.add(course)

                        }
                        count++
                        semester.courses = (courses)

                        if (count > semesters.size - 1)
                            updateRecycler()

                    }

                    .addOnFailureListener { exception ->
                        Log.w(TAG, "Error getting documents: ", exception)
                        count++

                    }
            }
        }
    }

    private fun fetchTranscript(callback: (ArrayList<TranscriptListItem>) -> Unit) {
        val semesterQuery = db.collection("semesters")
            .whereEqualTo("userID", auth.currentUser?.uid)
            .whereEqualTo("completed", true)
            .orderBy("timestamp", Query.Direction.DESCENDING)

        semesterQuery.addSnapshotListener { value, e ->

            if (e != null) {
                Log.w(TAG, "Listen failed.", e)
                progressBar.hide()
                infoText.text = "No Completed Semesters"
                return@addSnapshotListener
            }


            val semesters: ArrayList<TranscriptListItem> = ArrayList()

            if (value!!.isEmpty) {
                progressBar.hide()
                infoText.text = "No Completed Semesters"
            } else {
                //emptyView.visibility = View.GONE

                println(value.documents.toString())

                for ((position, doc) in value.withIndex()) {

                    val semester = TranscriptListItem(
                        semesterID = doc.getString("semesterID")!!,
                        semesterName = doc.getString("semesterName")!!,
                        timestamp = doc.getLong("timestamp")!!,
                        section = position,
                        courses = null
                    )

                    semesters.add(semester)

                }
                callback(semesters)
            }
        }
    }

    private fun updateRecycler() {
        transcriptList.sortByDescending { it.timestamp }
        val transcriptMap = mutableMapOf<String, ArrayList<CourseListItem>?>()
        for (semester in transcriptList) {
            println(semester.courses)
            transcriptMap[semester.semesterName] = semester.courses
        }
        println(transcriptMap.toString())
        var count = 0
        for ((key, value) in transcriptMap) {
            if (value != null) {
                if (value.size > 0) {
                    sectionedAdapter!!.addSection(
                        TranscriptSection(
                            key,
                            count,
                            value,
                            this
                        )
                    )
                }
            } else {
                val emptyArray = ArrayList<CourseListItem>()
                sectionedAdapter!!.addSection(
                    TranscriptSection(
                        key,
                        count,
                        emptyArray,
                        this
                    )
                )
            }
            count++
        }
        gpaText.text = "Cumulative GPA: ${calculateGPA()}"
        gpaText.visibility = View.VISIBLE
        sectionedAdapter?.notifyDataSetChanged()
        progressBar.hide()
        infoText.visibility = View.GONE
    }

    private fun calculateGPA(): String {
        val df = DecimalFormat("0.00")
        var totalValue = 0.0
        var points = 0.0
        for ((i, _) in gradesList.withIndex()) {
            if (gradesList[i] > -1) {
                when {
                    gradesList[i] == 0.1 -> {
                        totalValue += hoursList[i].toDouble() * 0.0
                        points += hoursList[i].toDouble()
                    }
                    gradesList[i] == 5.0 -> {
                        totalValue += hoursList[i].toDouble() * 5.0
                        points += hoursList[i].toDouble()
                    }
                    else -> {
                        totalValue += hoursList[i].toDouble() * gradesList[i]
                        points += hoursList[i].toDouble()
                    }
                }

            }
        }

        return df.format(((totalValue / points)))

    }

    override fun onHeaderRootViewClicked(section: TranscriptSection) {
        val sectionAdapter: SectionAdapter? = sectionedAdapter?.getAdapterForSection(section)

        // store info of current section state before changing its state
        val wasExpanded: Boolean = section.isExpanded
        val previousItemsTotal: Int = section.contentItemsTotal
        section.isExpanded = !wasExpanded
        sectionAdapter?.notifyHeaderChanged()
        if (wasExpanded) {
            sectionAdapter?.notifyItemRangeRemoved(0, previousItemsTotal)
        } else {
            sectionAdapter?.notifyAllItemsInserted()
        }
    }

    override fun onItemRootViewClicked(
        sectionTitle: String,
        itemAdapterPosition: Int
    ) {
        Toast.makeText(
            context, String.format(
                "Clicked on position #%s of Section %s",
                sectionedAdapter!!.getPositionInSection(itemAdapterPosition),
                sectionTitle
            ),
            Toast.LENGTH_SHORT
        ).show()
    }

    companion object {

        private const val TAG = "TranscriptFragment"


    }
}