package com.walentsoftware.schoolcompanion.activities.course.view.upcoming

import android.app.SearchManager
import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import android.util.Log
import android.view.*
import android.widget.TextView
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.SearchView
import androidx.core.view.updatePaddingRelative
import androidx.core.widget.ContentLoadingProgressBar
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.snackbar.Snackbar
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.Query
import com.walentsoftware.schoolcompanion.R
import com.walentsoftware.schoolcompanion.activities.assignment.AssignmentDetailActivity
import com.walentsoftware.schoolcompanion.activities.assignment.CreateAssignmentActivity
import com.walentsoftware.schoolcompanion.activities.course.EditCourseActivity
import com.walentsoftware.schoolcompanion.adapter.upcoming.CourseUpcomingAssignmentsAdapter
import com.walentsoftware.schoolcompanion.databinding.FragmentUpcomingBinding
import com.walentsoftware.schoolcompanion.extension.clearDecorations
import com.walentsoftware.schoolcompanion.extension.doOnApplyWindowInsets
import com.walentsoftware.schoolcompanion.extension.viewModelProvider
import com.walentsoftware.schoolcompanion.helper.PrimaryActionModeCallback
import com.walentsoftware.schoolcompanion.interfaces.CourseUpcomingAssignmentListener
import com.walentsoftware.schoolcompanion.model.data.CourseUpcomingAssignment
import com.walentsoftware.schoolcompanion.model.view.assignment.CourseUpcomingAssignmentViewModel
import com.walentsoftware.schoolcompanion.model.view.assignment.CourseUpcomingAssignmentViewModelFactory
import kotlinx.android.synthetic.main.fragment_upcoming.*


class UpcomingFragment : Fragment(), CourseUpcomingAssignmentListener {

    private lateinit var auth: FirebaseAuth
    private lateinit var db: FirebaseFirestore
    private lateinit var courseID: String

    private lateinit var viewModelFactory: ViewModelProvider.Factory
    private lateinit var viewModel: CourseUpcomingAssignmentViewModel
    private lateinit var binding: FragmentUpcomingBinding
    private lateinit var recyclerView: RecyclerView
    private var list: ArrayList<CourseUpcomingAssignment> = ArrayList()
    private var snackbar: Snackbar? = null

    private lateinit var assignmentAdapter: CourseUpcomingAssignmentsAdapter

    private lateinit var progressBar: ContentLoadingProgressBar
    private lateinit var emptyView: TextView

    private var actionMode = PrimaryActionModeCallback()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        setHasOptionsMenu(true)
        courseID = arguments?.getString("courseID").toString()
        binding = FragmentUpcomingBinding.inflate(inflater, container, false).apply {
            lifecycleOwner = viewLifecycleOwner
        }
        //Pad the bottom of the RecyclerView so that the content scrolls up above the nav bar
        binding.recyclerView.doOnApplyWindowInsets { v, insets, padding ->
            v.updatePaddingRelative(bottom = padding.bottom + insets.systemWindowInsetBottom)
        }
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceBundle: Bundle?) {
        super.onViewCreated(view, savedInstanceBundle)

        auth = FirebaseAuth.getInstance()

        db = FirebaseFirestore.getInstance()


        progressBar = view.findViewById(R.id.progress_circular)
        emptyView = view.findViewById(R.id.emptyView)

        actionMode.onActionItemClickListener = ::onActionModeItemClicked
        actionMode.onActionModeFinishedListener = ::finishActionMode


        if(courseID.isEmpty()) {

            activity?.finish()
        } else {
            fetchAssignments()
        }

        recyclerView = view.findViewById(R.id.recyclerView)
        assignmentAdapter = CourseUpcomingAssignmentsAdapter(this, list)

        if (recyclerView.adapter == null) {
            recyclerView.adapter = assignmentAdapter
        }

        snackbar = Snackbar.make(container, "Awaiting user action.", Snackbar.LENGTH_LONG)

    }

    private fun updateHeaders() {
        recyclerView.clearDecorations()
        if (list.isNotEmpty()) {
            recyclerView.addItemDecoration(
                AssignmentHeadersDecoration(recyclerView.context, list)
            )
        }
    }


    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        viewModelFactory = CourseUpcomingAssignmentViewModelFactory()



        viewModel = viewModelProvider(viewModelFactory)

        binding.viewModel = viewModel

    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        // Do something that differs the Activity's menu here

        inflater.inflate(R.menu.fragment_upcoming, menu)

        // Associate searchable configuration with the SearchView
        val searchManager =
            requireActivity().getSystemService(Context.SEARCH_SERVICE) as SearchManager
        val searchView = menu.findItem(R.id.action_search)
            .actionView as SearchView
        searchView.setSearchableInfo(
            searchManager.getSearchableInfo(requireActivity().componentName)
        )
        searchView.maxWidth = Int.MAX_VALUE

        // listening to search query text change
        searchView.setOnQueryTextListener(object :
            SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String): Boolean {
                // filter recycler view when query submitted
                assignmentAdapter.filter?.filter(query)
                return false
            }

            override fun onQueryTextChange(query: String): Boolean {
                // filter recycler view when text is changed
                assignmentAdapter.filter?.filter(query)
                return false
            }
        })

    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.action_info ->{
//                val intent = Intent(activity, CreateCourseActivity::class.java)
//                startActivity(intent)
                return true
            }
            R.id.action_delete -> {

                return true
            }
            R.id.action_search -> {

            }
            R.id.action_add -> {

                val intent = Intent(activity, CreateAssignmentActivity::class.java)
                intent.putExtra("courseID", courseID)
                startActivity(intent)
                return true
            }
            R.id.action_edit -> {
                val intent = Intent(activity, EditCourseActivity::class.java)
                intent.putExtra("courseID", courseID)
                startActivity(intent)
                return true
            }
        }
        return super.onOptionsItemSelected(item)
    }

    private fun fetchAssignments() {
        Log.w(TAG, "Course Id is. $courseID")
        val assignmentsQuery = db.collection("assignments")
            .whereEqualTo("userID", auth.currentUser?.uid)
            .whereEqualTo("courseID", courseID)
            .whereEqualTo("completed", false)
            .orderBy("dueDate", Query.Direction.ASCENDING)

        assignmentsQuery.addSnapshotListener { value, e ->

            if ((progressBar.visibility == View.INVISIBLE) || (progressBar.visibility == View.GONE))
                progressBar.show()

            if (e != null) {
                Log.w(TAG, "Listen failed.", e)
                emptyView.text = e.toString()
                progressBar.hide()
                emptyView.visibility = View.VISIBLE
                list.clear()
                assignmentAdapter.notifyDataSetChanged()
                updateHeaders()
                return@addSnapshotListener
            }


            if (value!!.isEmpty) {
                progressBar.hide()
            } else {
                //emptyView.visibility = View.GONE
                list.clear()

                for (doc in value) {

                    val description = doc.getString("notes")
                    var desc = doc.getString("notes")

                    if (description.isNullOrBlank() || description.isNullOrBlank()) {
                        desc = "No Notes"
                    }

                    var color: Int
                    var strokeColor: Int
                    var isDark = false

                    val priority = doc.getLong("priority")!!.toInt()

                    when (priority) {
                        0 -> {
                            color = 0xff5bb975.toInt()
                            strokeColor = Color.parseColor("#7fc8a5")
                            isDark = true
                        }
                        1 -> {
                            color = 0xfffbbc05.toInt()
                            strokeColor = Color.parseColor("#fcc937")
                        }
                        2 -> {
                            color = Color.parseColor("#fc3737")
                            strokeColor = Color.parseColor("#fd6969")
                            isDark = true
                        }
                        else -> {
                            color = 0xff5bb975.toInt()
                            strokeColor = Color.parseColor("#7fc8a5")
                            isDark = true
                        }
                    }

                    list.add(
                        CourseUpcomingAssignment(
                            id = doc.getString("assignmentID")!!,
                            name = doc.getString("assignmentName")!!,
                            dueDate = doc.getLong("dueDate")!!,
                            priority = priority,
                            desc = desc,
                            color = color,
                            isDark = isDark,
                            strokeColor = strokeColor,
                            timestamp = doc.getLong("timestamp")!!
                        )
                    )
                }
                assignmentAdapter.notifyDataSetChanged()
                updateHeaders()
                emptyView.visibility = View.GONE
                progressBar.hide()
            }
        }
    }

    override fun onRowClicked(position: Int) {

        println("row $position was clicked")

        if (assignmentAdapter.getSelectedItemCount() > 0) {
            enableActionMode(position)
        } else {
            // read the message which removes bold from the row
            val intent = Intent(activity, AssignmentDetailActivity::class.java)
            intent.putExtra("ID", list[position].id)
            startActivity(intent)
        }
    }


    override fun onIconClicked(position: Int) {
        println("icon clicked in the $position position")

        enableActionMode(position)
    }

    override fun onLongRowClicked(position: Int) {
        println("row $position was long clicked")
        enableActionMode(position)
    }

    private fun onActionModeItemClicked(item: MenuItem) {
        when (item.itemId) {
            R.id.action_delete -> deleteAssignments()

        }
    }

    private fun toggleSelection(position: Int) {
        assignmentAdapter.toggleSelection(position)
        val count: Int = assignmentAdapter.getSelectedItemCount()
        if (count == 0) {
            finishActionMode()
        }
        actionMode.selectedItemsCount = count
    }

    private fun enableActionMode(position: Int) {
        startActionMode()
        toggleSelection(position)
    }

    private fun startActionMode() {
        if (actionMode.isActive) return


        (activity as AppCompatActivity?)!!.supportActionBar!!.hide()
        actionMode.startActionMode(requireActivity(), R.menu.upcoming_menu_action_mode)
        //activity?.actionBar?.hide()
    }

    private fun finishActionMode() {
        if (!actionMode.isActive) return

        assignmentAdapter.clearSelections()
        (activity as AppCompatActivity?)!!.supportActionBar!!.show()
        actionMode.finishActionMode()

    }

    private fun deleteAssignments() {
        val dialogBuilder = AlertDialog.Builder(requireContext())

        // set message of alert dialog
        dialogBuilder.setMessage("Are you sure you want to delete the assignment(s)? This action cannot be undone.")
            // if the dialog is cancelable
            .setCancelable(false)
            // positive button text and action
            .setPositiveButton("OK") { _, _ ->
                confirmedDelete()
            }
            .setNegativeButton("Cancel") { _, _ ->
            }

        // create dialog box
        val alert = dialogBuilder.create()
        // set title for alert dialog box
        alert.setTitle("Confirm")
        // show alert dialog
        alert.show()


    }

    private fun confirmedDelete() {
        val batch = db.batch()



        for (i in (assignmentAdapter.getSelectedItems()?.size?.minus(1))?.downTo(0)!!) {
            val assignmentRef = db.collection("assignments").document(list[i].id)
            batch.delete(assignmentRef)
            // list.removeAt(i)
        }

        batch.commit()
            .addOnSuccessListener {
//                for (i in (selected.size.minus(1)).downTo(0)) {
//                    list.removeAt(i)
//                }
                Log.w(TAG, "deletion good")
            }
            .addOnFailureListener { e ->
                Log.w(TAG, e.toString())
                showSnackbar(e.toString())
            }

        //assignmentAdapter.notifyDataSetChanged()
    }

    private fun showSnackbar(text: CharSequence) {
        snackbar?.setText(text)
        snackbar!!.show()
    }

    companion object {

        private const val TAG = "Upcoming Fragment"

    }




}