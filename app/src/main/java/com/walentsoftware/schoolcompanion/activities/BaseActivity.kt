package com.walentsoftware.schoolcompanion.activities

import android.app.Activity
import android.app.ProgressDialog
import android.content.Context
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.ProgressBar
import android.widget.RelativeLayout
import androidx.appcompat.app.AppCompatActivity

open class BaseActivity : AppCompatActivity(){

    private var progressBar: ProgressBar? = null
    private var background: RelativeLayout? = null
    private var context: Activity? = null
    private var dialog: ProgressDialog? = null

    fun setProgressBar(resId: Int) {
        progressBar = findViewById(resId)
    }

    fun setBackground(resId: Int) {
        background = findViewById(resId)
    }

    fun setActivity(activity: Activity) {
        context = activity
    }

    fun showProgressBar() {
        if (progressBar != null && background != null) {
            progressBar?.visibility = View.VISIBLE
            background?.visibility = View.VISIBLE
        }
    }

    fun showDialog() {

        dialog = ProgressDialog.show(
            context, "",
            "Loading. Please wait...", true
        )


        println(context)
    }

    fun hideDialog() {
        dialog?.hide()
    }

    fun hideProgressBar() {
        progressBar?.visibility = View.INVISIBLE
        background?.visibility = View.INVISIBLE
    }

    fun hideKeyboard(view: View) {
        val imm = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        imm.hideSoftInputFromWindow(view.windowToken, 0)
    }

    public override fun onStop() {
        super.onStop()
        hideProgressBar()
        hideDialog()
    }

}