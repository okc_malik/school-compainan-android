package com.walentsoftware.schoolcompanion.activities.home.ui.settings

import android.content.Intent
import android.os.Bundle
import android.text.TextUtils
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import com.google.android.material.snackbar.Snackbar
import com.google.firebase.auth.EmailAuthProvider
import com.google.firebase.auth.FirebaseAuth
import com.walentsoftware.schoolcompanion.App
import com.walentsoftware.schoolcompanion.R
import com.walentsoftware.schoolcompanion.activities.splash.LoginActivity
import kotlinx.android.synthetic.main.activity_edit_email.*
import kotlinx.android.synthetic.main.content_edit_email.*


class EditEmailActivity : AppCompatActivity() {

    private lateinit var auth: FirebaseAuth
    private var snackbar: Snackbar? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_edit_email)
        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowHomeEnabled(true)

        auth = FirebaseAuth.getInstance()

        email.editText?.setText(auth.currentUser?.email)

        val contextView = findViewById<View>(R.id.coordinator)
        snackbar = Snackbar.make(contextView, "Awaiting user action.", Snackbar.LENGTH_LONG)

        (application as App).preferenceRepository
            .nightModeLive.observe(this, Observer { nightMode ->
                nightMode?.let { delegate.localNightMode = it }
            }
            )

    }

    public override fun onStart() {
        super.onStart()
        // Check if user is signed in (non-null) and update UI accordingly.
        val currentUser = auth.currentUser

        if (currentUser == null) {

            val intent = Intent(this, LoginActivity::class.java)
            startActivity(intent)
            finish()
        }
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.done, menu)
        return true

    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.action_done -> {
                updateEmail()
                return true
            }

        }
        return super.onOptionsItemSelected(item)
    }

    private fun updateEmail() {
        if (!validateForm())
            return

        val passwordString = password.editText?.text.toString()
        val credential = EmailAuthProvider
            .getCredential(
                auth.currentUser?.email!!,
                passwordString
            ) // Current Login Credentials \\

        auth.currentUser?.reauthenticate(credential)
            ?.addOnSuccessListener {
                Log.d(TAG, "User re-authenticated.")

                auth.currentUser?.updateEmail(email.editText?.text.toString())
                    ?.addOnSuccessListener { e ->
                        if (e != null) {
                            showSnackbar(e.toString())
                        } else {
                            Log.d(TAG, "User email address updated.")
                            FirebaseAuth.getInstance().signOut()
                            finish()
                        }


                    }
                    ?.addOnFailureListener { e ->
                        Log.w(TAG, "Error deleting document", e)
                        showSnackbar(e.toString())
                    }
            }
            ?.addOnFailureListener { e ->
                Log.w(TAG, "Error deleting document", e)
                showSnackbar(e.toString())
            }


    }


    private fun validateForm(): Boolean {
        var valid = true

        val emailString = email.editText?.text.toString()
        if (TextUtils.isEmpty(emailString)) {
            email.error = "Required."
            valid = false
        } else {
            email.error = null
        }

        val passwordString = password.editText?.text.toString()
        if (TextUtils.isEmpty(passwordString)) {
            password.error = "Required."
            valid = false
        } else {
            password.error = null
        }

        return valid
    }

    private fun showSnackbar(text: CharSequence) {
        snackbar?.setText(text)
        snackbar!!.show()
    }

    companion object {
        private const val TAG = "Edit Email"
    }
}