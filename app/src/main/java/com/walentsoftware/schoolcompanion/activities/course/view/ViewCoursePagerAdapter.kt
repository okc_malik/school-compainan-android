package com.walentsoftware.schoolcompanion.activities.course.view

import android.content.Context
import android.os.Bundle
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentStatePagerAdapter
import com.walentsoftware.schoolcompanion.R
import com.walentsoftware.schoolcompanion.activities.course.view.completed.CompletedFragment
import com.walentsoftware.schoolcompanion.activities.course.view.upcoming.UpcomingFragment


class ViewCoursePagerAdapter (
    private val context: Context,
    fragmentManager: FragmentManager,
    val courseID: String?
) : FragmentStatePagerAdapter(fragmentManager) {

    enum class MainFragments(val titleRes: Int) {
        UPCOMING(R.string.tab_text_1),
        COMPLETED(R.string.tab_text_2)
    }

    override fun getCount(): Int = MainFragments.values().size

    private fun getItemType(position: Int): MainFragments {
        return MainFragments.values()[position]
    }

    override fun getPageTitle(position: Int): CharSequence? {
        return context.getString(getItemType(position).titleRes)
    }

    override fun getItem(position: Int): Fragment {
        return when (getItemType(position)) {
            MainFragments.UPCOMING -> {
                val bundle = Bundle()
                bundle.putString("courseID", courseID)
                val upcomingFragment: Fragment = UpcomingFragment()
                upcomingFragment.arguments = bundle
                return upcomingFragment
            }
            MainFragments.COMPLETED -> {
                val bundle = Bundle()
                bundle.putString("courseID", courseID)
                val completedFragment: Fragment = CompletedFragment()
                completedFragment.arguments = bundle
                return completedFragment
            }
        }
    }
}