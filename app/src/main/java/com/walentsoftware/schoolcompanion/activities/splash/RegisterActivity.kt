package com.walentsoftware.schoolcompanion.activities.splash

import android.content.Intent
import android.os.Bundle
import android.text.TextUtils
import android.util.Log
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import com.google.android.material.button.MaterialButton
import com.google.android.material.snackbar.Snackbar
import com.google.android.material.textfield.TextInputLayout
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.UserProfileChangeRequest
import com.walentsoftware.schoolcompanion.App
import com.walentsoftware.schoolcompanion.R
import com.walentsoftware.schoolcompanion.activities.home.HomeActivity
import kotlinx.android.synthetic.main.activity_register.*

class RegisterActivity : AppCompatActivity() {

    private lateinit var auth: FirebaseAuth

    private var nameTextView: TextInputLayout? = null
    private var emailTextView: TextInputLayout? = null
    private var passwordTextView: TextInputLayout? = null
    private var confPasswordTextView: TextInputLayout? = null
    private var loginButton: MaterialButton? = null
    private var registerButton: MaterialButton? = null
    private var snackbar: Snackbar? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_register)

        nameTextView = findViewById(R.id.nameTextView)
        emailTextView = findViewById(R.id.emailTextView)
        passwordTextView = findViewById(R.id.passwordTextField)
        confPasswordTextView = findViewById(R.id.confPasswordTextField)
        loginButton = findViewById(R.id.loginButton)
        registerButton = findViewById(R.id.createAccountButton)

        auth = FirebaseAuth.getInstance()

        snackbar = Snackbar.make(coordinator, "Awaiting user action.", Snackbar.LENGTH_LONG)

        registerButton?.setOnClickListener {
            createUser(
                emailTextView?.editText?.text.toString(),
                passwordTextView?.editText?.text.toString()
            )
        }

        loginButton?.setOnClickListener {
            finish()
        }

        (application as App).preferenceRepository
            .nightModeLive.observe(this, Observer { nightMode ->
                nightMode?.let { delegate.localNightMode = it }
            }
            )

    }

    public override fun onStart() {
        super.onStart()
        // Check if user is signed in (non-null) and update UI accordingly.
        val currentUser = auth.currentUser
        //updateUI(currentUser)

        if (currentUser != null) {

            val intent = Intent(this, HomeActivity::class.java)
            startActivity(intent)
            finish()
        }
    }

    private fun createUser(email: String, password: String) {

        Log.d(TAG, "signIn:$email")
        if (!validateForm()) {

            return
        }


        auth.createUserWithEmailAndPassword(email, password)
            .addOnCompleteListener(this) { task ->
                if (task.isSuccessful) {
                    // Sign in success, update UI with the signed-in user's information
                    Log.d(TAG, "createUserWithEmail:success")
                    val user = auth.currentUser
                    val profileUpdates = UserProfileChangeRequest.Builder()
                        .setDisplayName(nameTextView?.editText?.text.toString())
                        .build()

                    user?.updateProfile(profileUpdates)
                        ?.addOnCompleteListener { profileTask ->
                            if (profileTask.isSuccessful) {
                                Log.d(TAG, "User profile updated.")
                            }
                        }

                    user?.sendEmailVerification()
                        ?.addOnCompleteListener { emailTask ->
                            if (emailTask.isSuccessful) {
                                Log.d(TAG, "Email sent.")
                            }
                        }
                    finish()
                } else {
                    // If sign in fails, display a message to the user.
                    Log.w(TAG, "createUserWithEmail:failure", task.exception)
                    Toast.makeText(
                        baseContext, "Authentication failed.",
                        Toast.LENGTH_SHORT
                    ).show()
                    showSnackbar("Authentication failed.")
                }

                // ...
            }
    }

    private fun validateForm(): Boolean {
        var valid = true

        val name = nameTextView?.editText?.text.toString()
        if (TextUtils.isEmpty(name)) {
            nameTextView?.error = "Required."
            valid = false
        } else {
            nameTextView?.error = null
        }

        val email = emailTextView?.editText?.text.toString()
        if (TextUtils.isEmpty(email)) {
            emailTextView?.error = "Required."
            valid = false
        } else {
            emailTextView?.error = null
        }

        val password = passwordTextView?.editText?.text.toString()
        if (TextUtils.isEmpty(password)) {
            passwordTextView?.error = "Required."
            valid = false
        } else {
            passwordTextView?.error = null
        }

        val confPassword = confPasswordTextView?.editText?.text.toString()
        if (TextUtils.isEmpty(confPassword)) {
            confPasswordTextView?.error = "Required."
            valid = false
        } else {
            confPasswordTextView?.error = null
        }

        if (confPassword != password) {
            confPasswordTextView?.error = "Passwords do not match."
            passwordTextView?.error = "Passwords do not match."
            valid = false
        } else {
            confPasswordTextView?.error = null
            passwordTextView?.error = null
        }

        return valid
    }

    private fun showSnackbar(text: CharSequence) {
        snackbar?.setText(text)
        snackbar!!.show()
    }

    companion object {
        private const val TAG = "LoginActivity"
    }
}
