package com.walentsoftware.schoolcompanion.activities.assignment

import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import android.os.Bundle
import android.text.TextUtils
import android.util.Log
import android.util.TypedValue
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.TextView
import androidx.annotation.AttrRes
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.lifecycle.Observer
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.android.material.datepicker.CalendarConstraints
import com.google.android.material.datepicker.MaterialDatePicker
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import com.google.android.material.snackbar.Snackbar
import com.google.android.material.switchmaterial.SwitchMaterial
import com.google.android.material.textfield.TextInputLayout
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore
import com.walentsoftware.schoolcompanion.App
import com.walentsoftware.schoolcompanion.R
import com.walentsoftware.schoolcompanion.activities.splash.LoginActivity
import com.walentsoftware.schoolcompanion.extension.bind
import kotlinx.android.synthetic.main.activity_create_assignment.*
import kotlinx.android.synthetic.main.content_create_assignment.*
import java.util.*


class CreateAssignmentActivity : AppCompatActivity(), View.OnClickListener {

    private var priority = 0
    private var gained = 0.0
    private var worth = 0.0
    private var dueDate: Long? = null

    private lateinit var auth: FirebaseAuth
    private lateinit var db: FirebaseFirestore
    private lateinit var assignmentNameTextView: TextInputLayout
    private lateinit var priorityContainer: ConstraintLayout
    private lateinit var dueDateContainer: ConstraintLayout
    private lateinit var worthContainer: ConstraintLayout
    private lateinit var gainedContainer: ConstraintLayout
    private lateinit var completedSwitch: SwitchMaterial
    private lateinit var bottomSheet: View
    private lateinit var priorityLow: TextView
    private lateinit var priorityMedium: TextView
    private lateinit var priorityHigh: TextView
    private lateinit var priorityValue: TextView
    private lateinit var dueDateValue: TextView
    private var courseID: String? = null

    private var snackbar: Snackbar? = null
    private var today: Long = 0
    private var nextMonth: Long = 0
    private var janThisYear: Long = 0
    private var decThisYear: Long = 0
    private var oneYearForward: Long = 0
    private var todayPair: Pair<Long, Long>? = null
    private var nextMonthPair: Pair<Long, Long>? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_create_assignment)
        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowHomeEnabled(true)

        courseID = intent.getStringExtra("courseID")

        auth = FirebaseAuth.getInstance()
        db = FirebaseFirestore.getInstance()

        assignmentNameTextView = bind(R.id.assignmentNameTextView)
        completedSwitch = bind(R.id.completedSwitch)
        priorityContainer = bind(R.id.priorityContainer)
        dueDateContainer = bind(R.id.dueDateContainer)
        worthContainer = bind(R.id.worthContainer)
        gainedContainer = bind(R.id.gainedContainer)
        priorityLow = bind(R.id.priorityLow)
        priorityMedium = bind(R.id.priorityMedium)
        priorityHigh = bind(R.id.priorityHigh)
        priorityValue = bind(R.id.priorityValue)
        dueDateValue = bind(R.id.dueDateValue)
        bottomSheet = bind(R.id.bottom_drawer)

        priorityContainer.setOnClickListener(this)
        dueDateContainer.setOnClickListener(this)
        worthContainer.setOnClickListener(this)
        gainedContainer.setOnClickListener(this)
        priorityHigh.setOnClickListener(this)
        priorityMedium.setOnClickListener(this)
        priorityLow.setOnClickListener(this)

        val contextView = findViewById<View>(R.id.coordinator)
        snackbar = Snackbar.make(contextView, "Awaiting user action.", Snackbar.LENGTH_LONG)

        completedSwitch.setOnCheckedChangeListener { _, checked ->
            if (checked) {
                dueDateContainer.visibility = View.GONE
            } else {
                dueDateContainer.visibility = View.VISIBLE
            }
        }

        completedSwitch.isChecked = true

        (application as App).preferenceRepository
            .nightModeLive.observe(this, Observer { nightMode ->
                nightMode?.let { delegate.localNightMode = it }
            }
            )

    }

    public override fun onStart() {
        super.onStart()
        // Check if user is signed in (non-null) and update UI accordingly.
        val currentUser = auth.currentUser

        if (currentUser == null) {

            val intent = Intent(this, LoginActivity::class.java)
            startActivity(intent)
            finish()
        }
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.done, menu)
        return true

    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.action_done -> {
                createAssignment()
                return true
            }

        }
        return super.onOptionsItemSelected(item)
    }

    override fun onClick(v: View) {
        println(v.id)
        when (v.id) {
            R.id.dueDateContainer -> {
                initSettings()
                val builder: MaterialDatePicker.Builder<*> =
                    setupDateSelectorBuilder()
                val constraintsBuilder: CalendarConstraints.Builder =
                    setupConstraintsBuilder()
                builder.setTheme(resolveOrThrow(this, R.attr.materialCalendarFullscreenTheme))

                try {
                    builder.setCalendarConstraints(constraintsBuilder.build())
                    val picker: MaterialDatePicker<out Any> = builder.build()
                    addSnackBarListeners(picker)
                    picker.show(supportFragmentManager, picker.toString())
                } catch (e: IllegalArgumentException) {
                    snackbar?.setText(e.toString())
                    snackbar?.show()
                }
            }

            R.id.gainedContainer -> {
                val dialog = MaterialAlertDialogBuilder(this)
                    .setTitle("Points Gained")
                    .setView(R.layout.edit_text)
                    .setNegativeButton(resources.getString(R.string.cancel)) { dialog, which ->
                        dialog.dismiss()
                    }
                    .setPositiveButton(resources.getString(R.string.set)) { dialog, which ->
                        val input: TextView? =
                            (dialog as AlertDialog).findViewById(android.R.id.text1)
                        gained = input?.text.toString().toDouble()
                        gainedValue.text = input?.text.toString()
                        println(gained)
                        dialog.dismiss()
                    }
                    .show()
                val input: TextView? = (dialog as AlertDialog).findViewById(android.R.id.text1)
                input?.text = gained.toString()
            }
            R.id.worthContainer -> {
                val dialog = MaterialAlertDialogBuilder(this)
                    .setTitle("Assignment Worth")
                    .setView(R.layout.edit_text)
                    .setNegativeButton(resources.getString(R.string.cancel)) { dialog, which ->
                        dialog.dismiss()
                    }
                    .setPositiveButton(resources.getString(R.string.set)) { dialog, which ->
                        val input: TextView? =
                            (dialog as AlertDialog).findViewById(android.R.id.text1)
                        worth = input?.text.toString().toDouble()
                        worthValue.text = input?.text.toString()
                        dialog.dismiss()
                    }
                    .show()
                val input: TextView? = (dialog as AlertDialog).findViewById(android.R.id.text1)
                input?.text = worth.toString()
            }
            R.id.priorityContainer -> {
                val bottomSheetBehavior =
                    BottomSheetBehavior.from(bottomSheet)

                bottomSheetBehavior.state = BottomSheetBehavior.STATE_EXPANDED
            }
            R.id.priorityHigh -> {
                priority = 2
                priorityValue.text = resources.getString(R.string.high)
                val bottomSheetBehavior =
                    BottomSheetBehavior.from(bottomSheet)

                bottomSheetBehavior.state = BottomSheetBehavior.STATE_COLLAPSED
            }
            R.id.priorityMedium -> {
                priority = 1
                priorityValue.text = resources.getString(R.string.medium)
                val bottomSheetBehavior =
                    BottomSheetBehavior.from(bottomSheet)

                bottomSheetBehavior.state = BottomSheetBehavior.STATE_COLLAPSED
            }
            R.id.priorityLow -> {
                priority = 0
                priorityValue.text = resources.getString(R.string.low)
                val bottomSheetBehavior =
                    BottomSheetBehavior.from(bottomSheet)

                bottomSheetBehavior.state = BottomSheetBehavior.STATE_COLLAPSED
            }

        }
    }


    private fun getClearedUtc(): Calendar {
        val utc = Calendar.getInstance(TimeZone.getTimeZone("UTC"))
        utc.clear()
        return utc
    }

    private fun initSettings() {
        today = MaterialDatePicker.thisMonthInUtcMilliseconds()
        val calendar: Calendar = getClearedUtc()
        calendar.timeInMillis = today
        calendar.roll(Calendar.MONTH, 1)
        nextMonth = calendar.timeInMillis
        calendar.timeInMillis = today
        calendar[Calendar.MONTH] = Calendar.JANUARY
        janThisYear = calendar.timeInMillis
        calendar.timeInMillis = today
        calendar[Calendar.MONTH] = Calendar.DECEMBER
        decThisYear = calendar.timeInMillis
        calendar.timeInMillis = today
        calendar.roll(Calendar.YEAR, 1)
        oneYearForward = calendar.timeInMillis
        todayPair = Pair(today, today)
        nextMonthPair = Pair(nextMonth, nextMonth)
    }

    private fun setupDateSelectorBuilder(): MaterialDatePicker.Builder<*> {
        val builder = MaterialDatePicker.Builder.datePicker()
        builder.setSelection(today)
        builder.setInputMode(MaterialDatePicker.INPUT_MODE_CALENDAR)
        return builder

    }

    private fun setupConstraintsBuilder(): CalendarConstraints.Builder {
        val constraintsBuilder = CalendarConstraints.Builder()
        constraintsBuilder.setStart(janThisYear)
        constraintsBuilder.setEnd(decThisYear)
        constraintsBuilder.setOpenAt(today)

        return constraintsBuilder
    }

    private fun addSnackBarListeners(materialCalendarPicker: MaterialDatePicker<*>) {
        materialCalendarPicker.addOnPositiveButtonClickListener { selection: Any? ->
            snackbar!!.setText(materialCalendarPicker.headerText)
            snackbar!!.show()
            dueDate = materialCalendarPicker.selection as Long?
            dueDateValue.text = materialCalendarPicker.headerText
        }
        materialCalendarPicker.addOnNegativeButtonClickListener { dialog: View? ->
            snackbar?.setText("User Selected Cancel")
            snackbar!!.show()
        }
        materialCalendarPicker.addOnCancelListener { dialog: DialogInterface? ->
            snackbar?.setText("User set cancel")
            snackbar!!.show()
        }
    }


    private fun resolveOrThrow(context: Context, @AttrRes attributeResId: Int): Int {
        val typedValue = TypedValue()
        if (context.theme.resolveAttribute(attributeResId, typedValue, true)) {
            return typedValue.data
        }
        throw IllegalArgumentException(context.resources.getResourceName(attributeResId))
    }

    private fun createAssignment() {
        if (!validateForm()) {
            return
        }

        if (courseID == null) {
            snackbar?.setText("Error, please try again. Error Code: CA-NID")
            snackbar!!.show()
            return
        }

        val newAssignmentRef = db.collection("assignments").document()

        val docData = hashMapOf(
            "assignmentID" to newAssignmentRef.id,
            "assignmentName" to assignmentNameTextView.editText?.text.toString(),
            "courseID" to courseID,
            "userID" to auth.uid,
            "dueDate" to dueDate,
            "completed" to completedSwitch.isChecked,
            "pointsGained" to gained,
            "assignmentWorth" to worth,
            "notes" to notesTextView.editText?.text.toString(),
            "priority" to priority,
            "timestamp" to (Date().time / 1000)
        )
        newAssignmentRef
            .set(docData)
            .addOnSuccessListener {
                Log.d(TAG, "DocumentSnapshot written with ID: ${newAssignmentRef.id}")
                //hideDialog()
                finish()
            }
            .addOnFailureListener { e ->
                Log.w(TAG, "Error adding document", e)
                //hideDialog()
                val builder = android.app.AlertDialog.Builder(this)
                //set title for alert dialog
                builder.setTitle("Error")
                //set message for alert dialog
                builder.setMessage(e.toString())
                builder.setIcon(android.R.drawable.ic_dialog_alert)

                //performing positive action
                builder.setPositiveButton("Okay") { dialogInterface, _ ->
                    dialogInterface.dismiss()
                }
            }
    }

    private fun validateForm(): Boolean {
        var valid = true

        val email = assignmentNameTextView.editText?.text.toString()
        if (TextUtils.isEmpty(email)) {
            assignmentNameTextView.error = "Required."
            valid = false
        } else {
            assignmentNameTextView.error = null
        }

        if (!completedSwitch.isChecked) {

            if (dueDate == null) {
                valid = false
                snackbar?.setText("Please select a due date, or set the assignment to incomplete.")
                snackbar!!.show()
            }
        }

        return valid
    }

    companion object {
        private const val TAG = "Create Assignment"
    }
}
