package com.walentsoftware.schoolcompanion.activities.course.view.upcoming

import android.content.Context
import android.graphics.Canvas
import android.graphics.Paint
import android.graphics.Rect
import android.text.Layout
import android.text.StaticLayout
import android.text.TextPaint
import android.view.View
import androidx.core.content.res.*
import androidx.core.graphics.withTranslation
import androidx.core.view.forEach
import androidx.recyclerview.widget.RecyclerView
import com.walentsoftware.schoolcompanion.R
import com.walentsoftware.schoolcompanion.extension.newStaticLayout
import com.walentsoftware.schoolcompanion.model.data.CourseUpcomingAssignment
import java.text.SimpleDateFormat
import java.util.*
import java.util.concurrent.TimeUnit
import kotlin.math.abs
import kotlin.math.ceil


class AssignmentHeadersDecoration(
    context: Context,
    blocks: List<CourseUpcomingAssignment>
) : RecyclerView.ItemDecoration() {

    private val paint: TextPaint
    private val textWidth: Int
    private val decorHeight: Int
    private val verticalBias: Float

    init {
        val attrs = context.obtainStyledAttributes(
            R.style.DateHeaders,
            R.styleable.DateHeader
        )
        paint = TextPaint(Paint.ANTI_ALIAS_FLAG or Paint.SUBPIXEL_TEXT_FLAG).apply {
            color = attrs.getColorOrThrow(R.styleable.DateHeader_android_textColor)
            textSize = attrs.getDimensionOrThrow(R.styleable.DateHeader_android_textSize)
            try {
                typeface = ResourcesCompat.getFont(
                    context,
                    attrs.getResourceIdOrThrow(R.styleable.DateHeader_android_fontFamily)
                )
            } catch (_: Exception) {
                // ignore
            }
        }

        val displayMetrics = context.resources.displayMetrics
        //val dpHeight = displayMetrics.heightPixels / displayMetrics.density
        val dpWidth = displayMetrics.widthPixels / displayMetrics.density
        textWidth = dpWidth.toInt()
        val height = attrs.getDimensionPixelSizeOrThrow(R.styleable.DateHeader_android_height)
        val minHeight = ceil(paint.textSize).toInt()
        decorHeight = height.coerceAtLeast(minHeight)

        verticalBias = attrs.getFloat(R.styleable.DateHeader_verticalBias, 0.5f).coerceIn(0f, 1f)

        attrs.recycle()
    }

    // Get the block index:day and create header layouts for each
    private val daySlots: Map<Int, StaticLayout> =
        indexAssignmentHeaders(blocks).map {
            it.first to createHeader(context, it.second)
        }.toMap()

    override fun getItemOffsets(
        outRect: Rect,
        view: View,
        parent: RecyclerView,
        state: RecyclerView.State
    ) {
        val position = parent.getChildAdapterPosition(view)
        outRect.top = if (daySlots.containsKey(position)) decorHeight else 0
    }

    override fun onDraw(canvas: Canvas, parent: RecyclerView, state: RecyclerView.State) {
        val layoutManager = parent.layoutManager ?: return
        val centerX = parent.width / 3f

        parent.forEach { child ->
            if (child.top < parent.height && child.bottom > 0) {
                // Child is visible
                val layout = daySlots[parent.getChildAdapterPosition(child)]
                if (layout != null) {
                    val dx = (centerX - (layout.width / 2)) + 32
                    val dy = layoutManager.getDecoratedTop(child) +
                            child.translationY +
                            // offset vertically within the space according to the bias
                            (decorHeight - layout.height) * verticalBias
                    canvas.withTranslation(x = dx, y = dy) {
                        layout.draw(this)
                    }
                }
            }
        }
    }

    private fun createHeader(
        context: Context,
        time: Long
    ): StaticLayout {
        val format = SimpleDateFormat("EEEE, MMMM d, yyyy")


        val todayDate = Calendar.getInstance(TimeZone.getTimeZone("UTC"))

        val cal = Calendar.getInstance(TimeZone.getTimeZone("UTC"))
        cal.timeInMillis = time

        return when {
            calendarDaysBetween(todayDate, cal).toInt() == 0 -> {
                newStaticLayout(
                    "Today",
                    paint,
                    textWidth,
                    Layout.Alignment.ALIGN_NORMAL,
                    1f,
                    0f,
                    false
                )
            }
            calendarDaysBetween(todayDate, cal).toInt() == 1 -> {
                newStaticLayout(
                    "Tomorrow",
                    paint,
                    textWidth,
                    Layout.Alignment.ALIGN_NORMAL,
                    1f,
                    0f,
                    false
                )
            }
            else -> {
                newStaticLayout(
                    format.format(cal.time),
                    paint,
                    textWidth,
                    Layout.Alignment.ALIGN_NORMAL,
                    1f,
                    0f,
                    false
                )
            }
        }

    }

    private fun calendarDaysBetween(startCal: Calendar, endCal: Calendar): Long {

        val start = Calendar.getInstance()
        start.timeZone = startCal.timeZone
        start.timeInMillis = startCal.timeInMillis
        val end = Calendar.getInstance()
        end.timeZone = endCal.timeZone
        end.timeInMillis = endCal.timeInMillis

        // Set the copies to be at midnight, but keep the day information.
        start[Calendar.HOUR_OF_DAY] = 0
        start[Calendar.MINUTE] = 0
        start[Calendar.SECOND] = 0
        start[Calendar.MILLISECOND] = 0
        end[Calendar.HOUR_OF_DAY] = 0
        end[Calendar.MINUTE] = 0
        end[Calendar.SECOND] = 0
        end[Calendar.MILLISECOND] = 0

        // At this point, each calendar is set to midnight on
        // their respective days. Now use TimeUnit.MILLISECONDS to
        // compute the number of full days between the two of them.
        return TimeUnit.MILLISECONDS.toDays(
            abs(end.timeInMillis - start.timeInMillis)
        )
    }

}
