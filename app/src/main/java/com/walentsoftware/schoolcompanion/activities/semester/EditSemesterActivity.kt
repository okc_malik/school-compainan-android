package com.walentsoftware.schoolcompanion.activities.semester

import android.app.AlertDialog
import android.content.Intent
import android.os.Bundle
import android.text.TextUtils
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.core.widget.ContentLoadingProgressBar
import androidx.core.widget.NestedScrollView
import androidx.lifecycle.Observer
import com.google.android.material.button.MaterialButton
import com.google.android.material.snackbar.Snackbar
import com.google.android.material.switchmaterial.SwitchMaterial
import com.google.android.material.textfield.TextInputLayout
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.DocumentReference
import com.google.firebase.firestore.FirebaseFirestore
import com.walentsoftware.schoolcompanion.App
import com.walentsoftware.schoolcompanion.R
import com.walentsoftware.schoolcompanion.activities.splash.LoginActivity
import com.walentsoftware.schoolcompanion.data.SemesterListItem
import com.walentsoftware.schoolcompanion.extension.bind
import kotlinx.android.synthetic.main.activity_edit_semester.*

class EditSemesterActivity : AppCompatActivity() {
    private lateinit var auth: FirebaseAuth
    private lateinit var db: FirebaseFirestore
    private lateinit var semesterNameEditText: TextInputLayout
    private lateinit var completedSwitch: SwitchMaterial

    private lateinit var progressBar: ContentLoadingProgressBar
    private lateinit var loadingView: RelativeLayout
    private lateinit var infoView: RelativeLayout
    private lateinit var scrollView: NestedScrollView
    private lateinit var infoTextView: TextView
    private lateinit var retryButton: MaterialButton

    private var snackbar: Snackbar? = null

    private var semesterID: String? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_edit_semester)

        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowHomeEnabled(true)

        val contextView = findViewById<View>(R.id.coordinator)
        snackbar = Snackbar.make(contextView, "Awaiting user action.", Snackbar.LENGTH_LONG)

        (application as App).preferenceRepository
            .nightModeLive.observe(this, Observer { nightMode ->
                nightMode?.let { delegate.localNightMode = it }
            }
            )

        auth = FirebaseAuth.getInstance()
        db = FirebaseFirestore.getInstance()

        semesterNameEditText = bind(R.id.semesterNameTextView)
        completedSwitch = bind(R.id.completedSwitch)
        progressBar = bind(R.id.progress_circular)
        loadingView = bind(R.id.loadingView)
        infoView = bind(R.id.infoView)
        scrollView = bind(R.id.scrollView)
        infoTextView = bind(R.id.infoText)
        retryButton = bind(R.id.reloadButton)

        semesterID = intent.getStringExtra("semesterID")

        if (semesterID.isNullOrEmpty()) {
            progressBar.hide()
            loadingView.visibility = View.GONE
            infoTextView.text =
                "Error fetching your semester. Code: NSID-EC. Please go back and try again."
            infoView.visibility = View.VISIBLE
            retryButton.visibility = View.GONE
        } else {
            fetchSemester()
        }

    }

    public override fun onStart() {
        super.onStart()
        // Check if user is signed in (non-null) and update UI accordingly.
        val currentUser = auth.currentUser

        if (currentUser == null) {

            val intent = Intent(this, LoginActivity::class.java)
            startActivity(intent)
            finish()
        }
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.done, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.action_done -> {
                updateSemester()
                return true
            }

        }
        return super.onOptionsItemSelected(item)
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }

    private fun fetchSemester() {
        val docRef: DocumentReference = db.collection("semesters").document(semesterID!!)
        docRef.get()
            .addOnSuccessListener { document ->
                if (document != null) {
                    Log.d(TAG, "DocumentSnapshot data: ${document.data}")

                    val semester = SemesterListItem(
                        document.get("semesterID") as String,
                        document.get("semesterName") as String,
                        document.getBoolean("completed") as Boolean,
                        null, null,
                        document.getLong("timestamp")
                    )

                    updateValues(semester)
                } else {
                    Log.d(TAG, "No such document")
                    progressBar.hide()
                    loadingView.visibility = View.GONE
                    infoTextView.text = "No such document."
                    infoView.visibility = View.VISIBLE
                    retryButton.visibility = View.GONE
                }
            }
            .addOnFailureListener { exception ->
                Log.d(TAG, "get failed with ", exception)
                progressBar.hide()
                loadingView.visibility = View.GONE
                infoTextView.text = exception.toString()
                infoView.visibility = View.VISIBLE
                retryButton.visibility = View.GONE
            }
    }

    private fun updateValues(semester: SemesterListItem) {
        semesterNameEditText.editText?.setText(semester.name)
        completedSwitch.isChecked = semester.completed

        progressBar.hide()
        loadingView.visibility = View.GONE
        infoView.visibility = View.GONE
        retryButton.visibility = View.GONE
    }

    private fun updateSemester() {
        progressBar.show()
        loadingView.visibility = View.VISIBLE
        infoView.visibility = View.GONE
        retryButton.visibility = View.GONE

        if (!validateForm()) {
            progressBar.hide()
            loadingView.visibility = View.GONE
            infoView.visibility = View.GONE
            retryButton.visibility = View.GONE
            return
        }

        val semeserRef = db.collection("semesters").document(semesterID!!)

        semeserRef
            .update(
                "semesterName", semesterNameEditText.editText?.text.toString(),
                "completed", completedSwitch.isChecked
            )
            .addOnSuccessListener {
                Log.d(TAG, "DocumentSnapshot successfully updated!")
                progressBar.hide()
                loadingView.visibility = View.GONE
                infoView.visibility = View.GONE
                retryButton.visibility = View.GONE
                finish()
            }
            .addOnFailureListener { e ->
                progressBar.hide()
                loadingView.visibility = View.GONE
                infoView.visibility = View.GONE
                retryButton.visibility = View.GONE

                Log.w(TAG, "Error adding document", e)
                //hideDialog()
                val builder = AlertDialog.Builder(this)
                //set title for alert dialog
                builder.setTitle("Error")
                //set message for alert dialog
                builder.setMessage(e.toString())
                builder.setIcon(android.R.drawable.ic_dialog_alert)

                //performing positive action
                builder.setPositiveButton("Okay") { dialogInterface, _ ->
                    dialogInterface.dismiss()
                }
            }

    }


    private fun validateForm(): Boolean {
        var valid = true

        val email = semesterNameEditText.editText?.text.toString()
        if (TextUtils.isEmpty(email)) {
            semesterNameEditText.error = "Required."
            valid = false
        } else {
            semesterNameEditText.error = null
        }


        return valid
    }

    companion object {
        private const val TAG = "Edit Semester"
    }
}