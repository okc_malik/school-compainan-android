package com.walentsoftware.schoolcompanion.interfaces

import com.walentsoftware.schoolcompanion.data.course.ActiveCourse

interface ActiveCourseAdapterListener {
    fun onCourseRowClicked(position: Int)
}
interface ActiveCourseCallBack {
    fun onCallback(courses: ArrayList<ActiveCourse>?)
}

interface CourseUpcomingAssignmentListener {
    fun onRowClicked(position: Int)

    fun onIconClicked(position: Int)

    fun onLongRowClicked(position: Int)
}
