package com.walentsoftware.schoolcompanion.shared

import android.view.View
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.walentsoftware.schoolcompanion.R

internal class HeaderViewHolder(val rootView: View) :
    RecyclerView.ViewHolder(rootView) {
    val tvTitle: TextView
    val imgArrow: ImageView

    init {
        tvTitle = rootView.findViewById(R.id.tvTitle)
        imgArrow = rootView.findViewById(R.id.imgArrow)
    }
}
